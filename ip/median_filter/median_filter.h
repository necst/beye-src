#ifndef GAUSS_LIB
#define GAUSS_LIB

#include <ap_int.h>
#include <hls_stream.h>
#include <hls_video.h>
#include <stdint.h>
#include <stdlib.h>
#include "../parameters.h"
#include "../data_structure.h"

#define KERN_SIZE 3
#define WINDOW_SIZE (KERN_SIZE*KERN_SIZE)
#define CENTER (KERN_SIZE/2)


void median_filter( AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out );

#endif
