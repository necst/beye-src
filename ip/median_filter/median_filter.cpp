#include "median_filter.h"

#define CENTER 	          (KERN_SIZE/2)
#define WAIT_TIME         ((CENTER-1+ KERN_SIZE%2)*WIDTH + CENTER-(1-KERN_SIZE%2))
#define START_TIME        ((KERN_SIZE-1)*WIDTH+KERN_SIZE-1)
#define START_FLUSH       (WIDTH*(CENTER+1)-KERN_SIZE+CENTER+1)


hls::LineBuffer<KERN_SIZE,WIDTH,uint8_t> lineBuf;
hls::Window<KERN_SIZE,KERN_SIZE,uint8_t> pixels;

int pixCount = 0;
int outCount = 0;
int flush_pos=0;
int nmax = NMAX;

int count_last = 0;

uint8_t update_last(){	
	if(count_last==nmax-1){
		count_last=0;
		return 1;
	}else{
		count_last+=1;
		return 0;
	}	
}

uint8_t update_last_flush(){
	int last_nmax = update_last();
	int last_flush = (flush_pos==KERN_SIZE*WIDTH-1);
	return last_flush || last_nmax;
}


#define MIN(x,y) ( (x)>(y) ? (y) : (x) )
#define MAX(x,y) ( (x)>(y) ? (x) : (y) )

uint8_t compute_filter(hls::Window<KERN_SIZE,KERN_SIZE,uint8_t> & window){

	#pragma HLS inline

	int k,stage,i;
	uint8_t z[WINDOW_SIZE],t[WINDOW_SIZE];

	//copy window in z the first time

	for(int i=0;i<KERN_SIZE;i++)
		for(int j=0;j<KERN_SIZE;j++)
			z[i*KERN_SIZE+j] = window.getval(i,j);

	//sorting network

	for(stage=1; stage<=WINDOW_SIZE;stage++){
		if((stage%2)==1) k=0;
		if((stage%2)==0) k=1;

		for (i = k; i<WINDOW_SIZE	-1; i=i+2){
			t[i ] = MIN(z[i], z[i+1]);
			t[i+1] = MAX(z[i], z[i+1]);
			z[i ] = t[i ];
			z[i+1] = t[i+1];
		}

	}

	return z[WINDOW_SIZE/2];
	
}


void median_filter(AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out ){

	
	#pragma HLS ARRAY_PARTITION variable=&pixels.val dim=0
	#pragma HLS ARRAY_PARTITION variable=&lineBuf.val dim=1
	#pragma HLS RESOURCE variable=&lineBuf.val core=RAM_2P_BRAM

	MY_TYPE_VAL in,out;
	int pixCount = 0;
	int outCount = 0;

	count_last = 0;

	DMA_In >> in;
	nmax = in.data;
	in.last = 1;
	DMA_Out << in;

	rows:for(register int y=0; y<HEIGHT; y++){
		cols:for(register int x=0; x<WIDTH;x++){

			#pragma HLS pipeline II=1

			DMA_In >> in;


			pixels.shift_left();
			insert_col_window:for(int row=1;row<KERN_SIZE;row++){
				#pragma HLS unroll
				pixels.insert(lineBuf.getval(row,x),row-1,KERN_SIZE-1);
			}
			pixels.insert(in.data,KERN_SIZE-1,KERN_SIZE-1);
			
			lineBuf.shift_up(x);
			lineBuf.insert_top(in.data, x);

			//send upper vertical border of the image (same as received)
			if(pixCount<WAIT_TIME){
				out.data = in.data;
				//out.data = lineBuf.getval(WAIT_TIME/WIDTH+1,outCount%WIDTH);
			}else if(pixCount>=START_TIME){
				if(y>=KERN_SIZE-1 && x>=KERN_SIZE-1){
					out.data = compute_filter(pixels);
				}else{
					//border, equal to the current center (border pixel)
					out.data = pixels.getval(KERN_SIZE/2,KERN_SIZE/2);
				}
			}

			if(pixCount>WAIT_TIME-1){
				out.last = update_last();

				DMA_Out << out;

				outCount++;
			}

			pixCount++;
		}

	}

	//send lower vertical border of the image (same as received) flushing linebuffer
	//starting point is the last pixel filtered
	flush_buffer:for(flush_pos = START_FLUSH; flush_pos < WIDTH*KERN_SIZE; flush_pos++){
		#pragma HLS pipeline II=1
		//set last bit according to NMAX dma burst size
		out.last=update_last_flush();
		
		out.data = lineBuf.getval(flush_pos/WIDTH, flush_pos%WIDTH);
		outCount++;
		DMA_Out << out;
	}


}
	
