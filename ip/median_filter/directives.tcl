############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2013 Xilinx Inc. All rights reserved.
############################################################

set_directive_interface -mode ap_ctrl_none -register -latency 0 "median_filter"

set_directive_resource -core AXI4Stream -metadata {-bus_bundle DMA_Out} "median_filter" DMA_Out
set_directive_resource -core AXI4Stream -metadata {-bus_bundle DMA_In} "median_filter" DMA_In
