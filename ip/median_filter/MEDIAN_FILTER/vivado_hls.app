<AutoPilot:project xmlns:AutoPilot="com.autoesl.autopilot.project" projectType="C/C++" name="MEDIAN_FILTER" top="median_filter">
    <files>
        <file name="../../median_test.cpp" sc="0" tb="1" cflags=" "/>
        <file name="median_filter.cpp" sc="0" tb="false" cflags=""/>
        <file name="median_filter.h" sc="0" tb="false" cflags=""/>
        <file name="input.ppm" sc="0" tb="false" cflags=""/>
        <file name="../data_structure.cpp" sc="0" tb="false" cflags=""/>
    </files>
    <solutions>
        <solution name="solution1" status=""/>
    </solutions>
    <Simulation argv="">
        <SimFlow name="csim" setup="false" optimizeCompile="false" clean="false" ldflags="" mflags=""/>
    </Simulation>
</AutoPilot:project>

