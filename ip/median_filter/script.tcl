############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project MEDIAN_FILTER
set_top median_filter
add_files "median_filter.cpp median_filter.h input.ppm ../data_structure.cpp"
add_files -tb "median_test.cpp"
open_solution "solution1"

#zedboard
set_part {xc7z020clg484-1}    


create_clock -period 6 -name default
source "./directives.tcl"
csim_design
csynth_design
#cosim_design
export_design -format ip_catalog
