#include "circular_bound.h"

int outCount = 0;	//Number of the current output pixel
int count_last = 0; //current count of output pixels 
int nmax = NMAX;




/*
	Used to update the last condition, that is the value
	to be given to "last" flag of AxiStream protocol, for
	the output value.

	@returns  0 or 1
*/
uint8_t update_last(){
	int last = count_last;
	//resets the count and return the correct last value
	if(count_last==nmax-1){
		count_last=0;
		return 1;
	}else{
		count_last++;
		return 0;
	}	
}


/*
	Used to update the "last" condition, like update_last() function,
	but considering also the final pixel of the output image as last,
	in order to have a correct HW/SW alignment.

	@returns  0 or 1
*/
uint8_t update_last_flush(){
	int last_nmax = update_last();
	int last_flush = (outCount==WIDTH*HEIGHT-1);
	return last_flush || last_nmax;
}



/*
	Main function of the core, performs the filter.
	The pixels out of a fixed-size disc are blanked
*/

void circular_bound(AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out ){

	count_last=0;
	outCount=0;

	MY_TYPE_VAL in,out;
	
	DMA_In >> in;
	nmax = in.data;
	in.last = 1;
	DMA_Out << in;
	
	int val;
	
	int xc=WIDTH/2;
	int yc=HEIGHT/2;
	int r=xc-BORDER;
	
	rows:for(register int y=0; y<HEIGHT; y++){
		cols:for(register int x=0; x<WIDTH;x++){

			#pragma HLS pipeline II=1

			DMA_In >> in;

			if((y-yc)*(y-yc) + (x-xc)*(x-xc) >= r*r){
				out.data=255;				
			}else{
				out.data=in.data;
			}

			out.last = update_last_flush();

			DMA_Out << out;

			outCount++;
		}

	}
}




