#ifndef FILTER_LIB
#define FILTER_LIB


#include <ap_int.h>
#include <hls_stream.h>
#include <hls_video.h>
#include <stdint.h>
#include <stdlib.h>
#include "../parameters.h"
#include "../data_structure.h"

#define BORDER 32

/* Main core function prototype */
void circular_bound(AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out );

#endif
