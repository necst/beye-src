<AutoPilot:project xmlns:AutoPilot="com.autoesl.autopilot.project" projectType="C/C++" name="CIRCULAR_BOUND" top="circular_bound">
    <files>
        <file name="../../circular_bound-test.cpp" sc="0" tb="1" cflags=" "/>
        <file name="circular_bound.cpp" sc="0" tb="false" cflags=""/>
        <file name="circular_bound.h" sc="0" tb="false" cflags=""/>
        <file name="../data_structure.cpp" sc="0" tb="false" cflags=""/>
        <file name="input.ppm" sc="0" tb="false" cflags=""/>
    </files>
    <solutions>
        <solution name="solution1" status=""/>
    </solutions>
    <Simulation argv="">
        <SimFlow name="csim" setup="false" optimizeCompile="false" clean="false" ldflags="" mflags=""/>
    </Simulation>
</AutoPilot:project>

