#include "circular_bound.h"


#define RED 0
#define GREEN 1
#define BLUE 2

void print_matrix(short mat[][WIDTH]){
	int i,j;

	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			printf("%3d ",mat[i][j]);
		}
		printf("\n");
	}
}

//read a single channel from image
void load_ppm(char *filename, uint32_t mat[][WIDTH],int rgb){
	int i,j,is_rgb;
	char mode[2];
	uint32_t buf[3],pix;

	FILE *input = fopen(filename,"r");
	if(input==NULL){
		printf("Error in loading image %s\n",filename);
		exit(1);
	}

	fscanf(input,"%s %*d %*d %*d\n",mode);
	if(strcmp(mode,"P2")==0) is_rgb=0;
	else if(strcmp(mode,"P3")==0) is_rgb=1;
	else{
		printf("Image error, mode not present (either P2 or P3)\n");
		exit(1);
	}
	//Caricamento pixels
	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			
			fscanf(input,"%u",&buf[RED]);
			if(is_rgb){
				fscanf(input,"%u",&buf[GREEN]);
				fscanf(input,"%u",&buf[BLUE]);
			}

			if(is_rgb) pix=buf[rgb];
			else pix=buf[RED];

			mat[i][j]=pix;
		}
	}

	fclose(input);
}

void save_ppm(char *filename, uint32_t mat[][WIDTH]){
	int i,j;
	FILE *output = fopen(filename,"w");
	if(output==NULL){
		printf("Error in opening file %s in write mode\n",filename);
		exit(1);
	}

	fprintf(output,"P2 %d %d 255\n",WIDTH,HEIGHT);

	for(int i = 0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			fprintf(output,"%3u ",mat[i][j]);
		}
		fprintf(output,"\n");
	}

	fclose(output);
}

void copy_matrix(uint32_t in[][WIDTH],uint32_t out[][WIDTH]){
	for(int i=0;i<HEIGHT;i++)
		for(int j=0;j<WIDTH;j++)
			out[i][j]=in[i][j];
}

void print_matrix(uint32_t mat[][WIDTH]){
	int i,j;

	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			printf("%3d ",mat[i][j]);
		}
		printf("\n");
	}
}
int main(){
	int i,j,k,m,ok,lstok;

	hls::stream<AXI_VAL> in("input");
	hls::stream<AXI_VAL> out("output");

	AXI_VAL a,b;

	static uint32_t input[HEIGHT][WIDTH];
	static uint32_t output_soft[HEIGHT][WIDTH];
	static uint32_t output_hard[HEIGHT][WIDTH];
	static uint32_t tmp[HEIGHT][WIDTH];

	
	uint32_t nmax;
	
	ok = 1;
	lstok=1;

	//Randomly fills input matrix

	printf("Filling matrix\n");
	/*for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			input[i][j]=rand() % 255;
		}
	}*/

	load_ppm("input.ppm",input,GREEN);

	//run N_TESTS tests
	for(int test=0;test<N_TESTS;test++){
		printf("Test %d\n",test+1);

		nmax = rand()%NMAX;
		a.data = nmax;
		in.write(a);
	
		//sends data to core input and calls ip core
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				a.data = input[i][j];
				a.last = 0;
				in.write(a);
			}
		}

		//Main core function is called on the two streams
		circular_bound(in,out);
		
		//while(!out.empty());

		out.read(b);
		if(b.data != nmax){
			lstok=0;
			printf("Error in propagating NMAX parameter");
		}

		printf("Generating software output\n");
		
		//Get software output
		copy_matrix(input,output_soft);

		int th=0;
		int xc=WIDTH/2;
		int yc=HEIGHT/2;
		int r=xc-BORDER;
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){

				if((i-yc)*(i-yc) + (j-xc)*(j-xc) >= r*r) {
					tmp[i][j]=255;				
				
				}else{
					tmp[i][j]=input[i][j];	
				}
			}
		}
		
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				output_soft[i][j]=tmp[i][j];
			}
		}

		//Get Hardware output
		printf("Generating hardware output\n");
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				out.read(b);
				output_hard[i][j]=b.data;

				//Checks proper position of tlast bits (every NMAX integers and at last one)
				if(((i==HEIGHT-1 && j==WIDTH-1) || ((i*WIDTH+j+1)%NMAX==0 )) && b.last!=1){
					lstok = 0;
					printf("Error, last not set to 1 at [%d][%d]\n",i,j);
				}
			}
		}

		save_ppm("output_software.ppm",output_soft);
		save_ppm("output_hardware.ppm",output_hard);


	}

		
		

	printf("Comparing...\n");
	//Comparison bewteen SW and HW
	for(i=0;i<HEIGHT && ok;i++){
		for(j=0;j<WIDTH && ok;j++){
			if(output_soft[i][j]!=output_hard[i][j]){
				printf("\nfailed at [%d][%d] .... %d <> %d\n",i,j,output_soft[i][j],output_hard[i][j]);
				ok=0;
			}
		}
	}


	if(ok && lstok){
		printf("Test passed\n");
		return 0;
	}else{
		printf("Test failed\n");
		return 1;
	}

}