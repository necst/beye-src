#ifndef DATASTRUCT_LIB
#define DATASTRUCT_LIB
#include <stdio.h>
#include <ap_int.h>
#include <hls_stream.h>
#include <hls_video.h>
#define D_TYPE int

template<int D> struct ap_axis2{
    ap_uint<D> data;
    ap_uint<1> last;
};

struct my_ap_axis_struct{
    D_TYPE data;
    bool last;
};

typedef ap_axis2<32> AXI_VAL;

typedef struct my_ap_axis_struct MY_TYPE_VAL;

typedef hls::stream<AXI_VAL> AXI_STREAM_VAL;

MY_TYPE_VAL operator + (const MY_TYPE_VAL& lhs, const MY_TYPE_VAL &rhs);

MY_TYPE_VAL operator - (const MY_TYPE_VAL& lhs, const MY_TYPE_VAL &rhs);

MY_TYPE_VAL operator * (const D_TYPE& lhs, const MY_TYPE_VAL &rhs);

MY_TYPE_VAL operator * (const MY_TYPE_VAL &lhs, const D_TYPE& rhs);

MY_TYPE_VAL operator * (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs);

MY_TYPE_VAL operator / (const D_TYPE& lhs, const MY_TYPE_VAL &rhs);

MY_TYPE_VAL operator / (const MY_TYPE_VAL &lhs, const D_TYPE& rhs);

MY_TYPE_VAL operator / (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs);

bool operator == (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs);

bool operator == (const D_TYPE &lhs, const MY_TYPE_VAL& rhs);

bool operator == (const MY_TYPE_VAL &lhs, const D_TYPE& rhs);

bool operator < (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs);

bool operator <= (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs);

bool operator > (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs);

bool operator >= (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs);

bool operator > (const MY_TYPE_VAL &lhs, const D_TYPE& rhs);

bool operator >= (const MY_TYPE_VAL &lhs, const D_TYPE& rhs);
bool operator > (const D_TYPE& lhs, const MY_TYPE_VAL &rhs );
bool operator >= (const D_TYPE& lhs, const MY_TYPE_VAL &rhs );

bool operator < (const MY_TYPE_VAL &lhs, const D_TYPE& rhs);

bool operator <= (const MY_TYPE_VAL &lhs, const D_TYPE& rhs);

bool operator < (const D_TYPE& lhs, const MY_TYPE_VAL &rhs );

bool operator <= (const D_TYPE& lhs, const MY_TYPE_VAL &rhs );

D_TYPE operator - (const D_TYPE &lhs, const MY_TYPE_VAL& rhs);

D_TYPE operator - (const MY_TYPE_VAL &lhs, const D_TYPE& rhs);

D_TYPE operator + (const D_TYPE &lhs, const MY_TYPE_VAL& rhs);

D_TYPE operator + (const MY_TYPE_VAL &lhs, const D_TYPE& rhs);

D_TYPE operator % (const MY_TYPE_VAL &lhs, const D_TYPE &rhs);

void operator >> (AXI_STREAM_VAL& edge, MY_TYPE_VAL &data);

void operator << (AXI_STREAM_VAL& edge, MY_TYPE_VAL data);

#endif