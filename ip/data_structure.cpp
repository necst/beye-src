#include "data_structure.h"

MY_TYPE_VAL operator + (const MY_TYPE_VAL& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data + rhs.data;
	return temp;
}

MY_TYPE_VAL operator - (const MY_TYPE_VAL& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data - rhs.data;
	return temp;
}

MY_TYPE_VAL operator * (const D_TYPE& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs * rhs.data;
	return temp;
}

MY_TYPE_VAL operator * (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data * rhs;
	return temp;
}

MY_TYPE_VAL operator * (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data * rhs.data;
	return temp;
}

MY_TYPE_VAL operator / (const D_TYPE& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs / rhs.data;
	return temp;
}

MY_TYPE_VAL operator / (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data / rhs;
	return temp;
}

MY_TYPE_VAL operator / (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data / rhs.data;
	return temp;
}

bool operator == (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data == rhs.data;
}

bool operator == (const D_TYPE &lhs, const MY_TYPE_VAL& rhs) {
	return lhs == rhs.data;
}

bool operator == (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data == rhs;
}

bool operator < (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data < rhs.data;
}

bool operator <= (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data <= rhs.data;
}

bool operator > (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data > rhs.data;
}


bool operator >= (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data >= rhs.data;
}

bool operator > (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data > rhs;
}

bool operator >= (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data >= rhs;
}

bool operator > (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs > rhs.data;
}

bool operator >= (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs >= rhs.data;
}

bool operator < (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data < rhs;
}

bool operator <= (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data <= rhs;
}

bool operator < (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs < rhs.data;
}

bool operator <= (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs <= rhs.data;
}

D_TYPE operator - (const D_TYPE &lhs, const MY_TYPE_VAL& rhs) {
	return lhs - rhs.data;
}

D_TYPE operator - (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data - rhs;
}

D_TYPE operator + (const D_TYPE &lhs, const MY_TYPE_VAL& rhs) {
	return lhs + rhs.data;
}

D_TYPE operator + (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data + rhs;
}

D_TYPE operator % (const MY_TYPE_VAL &lhs, const D_TYPE &rhs) {
	return lhs.data % rhs;
}


void operator >> (AXI_STREAM_VAL& edge, MY_TYPE_VAL &data) {
	AXI_VAL temp;
	union
	{
		int ival;
		D_TYPE oval;
	} converter;

	edge >> temp;
	converter.ival = temp.data;
	data.data = converter.oval;
}

void operator << (AXI_STREAM_VAL& edge, MY_TYPE_VAL data) {
	AXI_VAL temp;
	union
	{
		int oval;
		D_TYPE ival;
	} converter;

	converter.ival = data.data;

	temp.data = converter.oval;
	temp.last = data.last;
	edge << temp;
}