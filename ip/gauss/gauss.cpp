#include "gauss.h"

#define WAIT_TIME ((KERN_SIZE-1)*WIDTH+KERN_SIZE)/2
#define START_TIME (WAIT_TIME*2)

const int coeffs[KERN_SIZE][KERN_SIZE]={
	{5,18,27,18,5},
	{18,59,87,59,18},
	{27,87,128,87,27},
	{18,59,87,59,18},
	{5,18,27,18,5}
};


hls::LineBuffer<KERN_SIZE,WIDTH,uint8_t> lineBuf;
hls::Window<KERN_SIZE,KERN_SIZE,uint8_t> pixels;

int pixCount = 0;
int outCount = 0;
int nmax = NMAX;
int count_last = 0;


uint8_t update_last(){
	
	if(count_last==nmax-1){
		count_last=0;
		return 1;
	}else{
		count_last+=1;
		return 0;
	}	
}

uint8_t update_last_flush(){
	int last_nmax = update_last();
	int last_flush = outCount==WIDTH*HEIGHT-1;
	return last_flush || last_nmax;
}

void gauss(AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out ){

	
	#pragma HLS ARRAY_PARTITION variable=&pixels.val dim=0
	#pragma HLS ARRAY_PARTITION variable=&lineBuf.val dim=1
	#pragma HLS RESOURCE variable=&lineBuf.val core=RAM_2P_BRAM

	MY_TYPE_VAL in,out;

	int sum;

	count_last = 0;
	pixCount = 0;
	outCount = 0;
	
	DMA_In >> in;
	nmax = in.data;
	DMA_Out << in;
	
	
	
	rows:for(register int y=0; y<HEIGHT; y++){
		cols:for(register int x=0; x<WIDTH;x++){

			#pragma HLS pipeline II=1

			DMA_In >> in;


			pixels.shift_left();
			insert_col_window:for(int row=1;row<KERN_SIZE;row++){
				#pragma HLS unroll
				pixels.insert(lineBuf.getval(row,x),row-1,KERN_SIZE-1);
			}
			pixels.insert(in.data,KERN_SIZE-1,KERN_SIZE-1);

			lineBuf.shift_up(x);
			lineBuf.insert_top(in.data, x);
			
			//send upper vertical border of the image (same as received)
			if(pixCount>WAIT_TIME-1 && pixCount<START_TIME){
				out.data = pixels.getval(WAIT_TIME/WIDTH,WAIT_TIME%WIDTH);
			}else if(pixCount>=START_TIME){
				if(y>=KERN_SIZE-1 && x>=KERN_SIZE-1){
					sum=0;
					for(int row=0;row<KERN_SIZE;row++){
						for(int col=0;col<KERN_SIZE;col++){
							#pragma HSL unroll
							sum+= (coeffs[row][col] * (int)pixels.getval(row,col))/1000;
						}
					}
					
					out.data = (uint8_t) sum;
				}else{
					//border, equal to the current center (border pixel)
					out.data = pixels.getval(KERN_SIZE/2,KERN_SIZE/2);
				}
			}

			if(pixCount>WAIT_TIME-1){
				out.last = update_last();

				DMA_Out << out;

				outCount++;
			}

			pixCount++;
		}

	}

	//send lower vertical border of the image (same as received) flushing linebuffer
	//starting point is the last pixel filtered
	int start = (KERN_SIZE-1)/2 * WIDTH + WIDTH - (KERN_SIZE-1)/2;
	flush_buffer:for(int pos = start; pos < WIDTH*KERN_SIZE; pos++){
		#pragma HLS pipeline II=1
		//set last bit according to NMAX dma burst size
		out.last = update_last_flush();
		out.data = lineBuf.getval(pos/WIDTH, pos%WIDTH);
		outCount++;
		DMA_Out << out;
	}


}
	
