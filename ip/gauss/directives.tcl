############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2013 Xilinx Inc. All rights reserved.
############################################################

set_directive_interface -mode ap_ctrl_none -register -latency 0 "gauss"

set_directive_resource -core AXI4Stream -metadata {-bus_bundle DMA_Out} "gauss" DMA_Out
set_directive_resource -core AXI4Stream -metadata {-bus_bundle DMA_In} "gauss" DMA_In
