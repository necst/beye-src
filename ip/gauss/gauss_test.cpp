#include "gauss.h"


#define RED 0
#define GREEN 1
#define BLUE 2


//read a single channel from image
void load_ppm(char *filename, uint32_t mat[][WIDTH],int rgb){
	int i,j,is_rgb;
	char mode[2];
	uint32_t buf[3],pix;

	FILE *input = fopen(filename,"r");
	if(input==NULL){
		printf("Error in loading image %s\n",filename);
		exit(1);
	}

	fscanf(input,"%s %*d %*d %*d\n",mode);
	if(strcmp(mode,"P2")==0) is_rgb=0;
	else if(strcmp(mode,"P3")==0) is_rgb=1;
	else{
		printf("Image error, mode not present (either P2 or P3)\n");
		exit(1);
	}
	//Caricamento pixels
	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			
			fscanf(input,"%u",&buf[RED]);
			if(is_rgb){
				fscanf(input,"%u",&buf[GREEN]);
				fscanf(input,"%u",&buf[BLUE]);
			}

			if(is_rgb) pix=buf[rgb];
			else pix=buf[RED];

			mat[i][j]=pix;
		}
	}

	fclose(input);
}

void copy_matrix(uint32_t in[][WIDTH],uint32_t out[][WIDTH]){
	for(int i=0;i<HEIGHT;i++)
		for(int j=0;j<WIDTH;j++)
			out[i][j]=in[i][j];
}

void save_ppm(char *filename, uint32_t mat[][WIDTH]){
	int i,j;
	FILE *output = fopen(filename,"w");
	if(output==NULL){
		printf("Error in opening file %s in write mode\n",filename);
		exit(1);
	}

	fprintf(output,"P2 %d %d 255\n",WIDTH,HEIGHT);

	for(int i = 0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			fprintf(output,"%3u ",mat[i][j]);
		}
		fprintf(output,"\n");
	}

	fclose(output);
}
	
void print_matrix(uint32_t mat[][WIDTH]){
	int i,j;

	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			printf("%3d ",mat[i][j]);
		}
		printf("\n");
	}
}

int main(){
	int i,j,k,m,ok,lstok;

	AXI_STREAM_VAL in;
	AXI_STREAM_VAL out;

	AXI_VAL a,b;

	static uint32_t input[HEIGHT][WIDTH];
	static uint32_t tmp[HEIGHT][WIDTH];
	static uint32_t output_hard[HEIGHT][WIDTH];
	static uint32_t output_soft[HEIGHT][WIDTH];

	const int coeffs[KERN_SIZE][KERN_SIZE]={
		{5,18,27,18,5},
		{18,59,87,59,18},
		{27,87,128,87,27},
		{18,59,87,59,18},
		{5,18,27,18,5}
	};

	int nmax = rand()%NMAX;

	ok = 1;
	lstok=1;

	//RIEMPIE A CASO MATRICE IMMAGINE

	printf("Filling matrix\n");
	/*for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			prova[i][j]=rand() % 255;
		}
	}*/
	
	load_ppm("input.ppm",input,GREEN);
	
	copy_matrix(input,output_soft);
	
	for(int test=0;test <N_TESTS;test++){
		printf("Test %d\n",test+1);


		//print_matrix(prova);
		a.data = NMAX;
		in.write(a);

		//Sends data to core input and calls ip core
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				a.data = input[i][j];
				in.write(a);
			}
		}

		gauss(in,out);
		
		out.read(b);
		if(b.data != NMAX){
			lstok=0;
			printf("Error in propagating NMAX parameter");
		}

		printf("Generating outputs\n");
		//Get software output
		for(i=CENTER;i<HEIGHT-CENTER;i++){
			for(j=CENTER;j<WIDTH-CENTER;j++){
				
				tmp[i][j]=0;

				for(int row = 0; row<KERN_SIZE;row++){
					for(int col=0;col<KERN_SIZE;col++){
						tmp[i][j]+=((int)coeffs[row][col] * (int)input[i-CENTER+row][j-CENTER+col])/1000;
					}
				}
			}	
		}
		for(i=CENTER;i<HEIGHT-CENTER;i++){
			for(j=CENTER;j<WIDTH-CENTER;j++){
				output_soft[i][j]=(short)tmp[i][j];
			}
		}

		//Get Hardware output
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				out.read(b);
				output_hard[i][j]=b.data;

				//Checks proper position of tlast bits (every NMAX integers and at last one)
				if(((i==HEIGHT-1 && j==WIDTH-1) || ((i*WIDTH+j+1)%NMAX==0 )) && b.last!=1){
					lstok = 0;
					printf("Error, last not set to 1 at [%d][%d]\n",i,j);
				}
			}
		}

		/*printf("Software output\n");
		print_matrix(prova);
		printf("\nHardware output\n");
		printf("\n");print_matrix(output);printf("\n");
		*/
		printf("Comparing...\n");
		//Comparison bewteen SW and HW
		for(i=0;i<HEIGHT && ok;i++){
			for(j=0;j<WIDTH && ok;j++){
				if(output_soft[i][j]!=output_hard[i][j]){
					printf("\nfailed at [%d][%d] .... %d <> %d\n",i,j,output_soft[i][j],output_hard[i][j]);
					ok=0;
				}
			}
		}
	}
	
	save_ppm("output_software.ppm",output_soft);
	save_ppm("output_hardware.ppm",output_hard);

	
	if(ok && lstok){
		printf("Test passed\n");
		return 0;
	}else{
		printf("Test failed\n");
		return 1;
	}

}

