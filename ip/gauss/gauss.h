#ifndef GAUSS_LIB
#define GAUSS_LIB

#include "/vivado/Vivado_HLS/2015.3/include/gmp.h"

#include <ap_int.h>
#include <hls_stream.h>
#include <hls_video.h>
#include <stdint.h>
#include <stdlib.h>
#include "../parameters.h"
#include "../data_structure.h"

#define KERN_SIZE 5
#define CENTER (KERN_SIZE/2)


void gauss( AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out );

#endif
