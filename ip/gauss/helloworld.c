/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>

#include "platform.h"
#include "xaxidma.h"
#include "xscutimer.h"
#include "xparameters.h"
#include "xtime_l.h"


#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif

#define NSECS_PER_SEC          667000000/2
#define TIMER_RES_DIVIDER      40
#define EE_TICKS_PER_SEC       (NSECS_PER_SEC / TIMER_RES_DIVIDER)
#define TIMER_LOAD_VALUE      0xFFFFFFE

int init_dma(XAxiDma *axiDmaPtr, int deviceId)
{
       XAxiDma_Config *CfgPtr;
       int status;
       // Get pointer to DMA configuration
       CfgPtr = XAxiDma_LookupConfig(deviceId);
       if(!CfgPtr){
               xil_printf("Error looking for AXI DMA config\n\r");
               return XST_FAILURE;
       }
       // Initialize the DMA handle
       status = XAxiDma_CfgInitialize(axiDmaPtr,CfgPtr);
       if(status != XST_SUCCESS){
               xil_printf("Error initializing DMA\n\r");
               return XST_FAILURE;
       }
       //check for scatter gather mode - this example must have simple mode only
       if(XAxiDma_HasSg(axiDmaPtr)){
               xil_printf("Error DMA configured in SG mode\n\r");
               return XST_FAILURE;
       }
       //disable the interrupts
       XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK,XAXIDMA_DEVICE_TO_DMA);
       XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK,XAXIDMA_DMA_TO_DEVICE);

       return XST_SUCCESS;
}

#define WIDTH 10
#define HEIGHT 10
#define N  WIDTH*HEIGHT
#define D_TYPE int
#define TSTEPS 1000
#define NMAX 20000

int main()
{

	int i=0;
	int input[N];
	int output[N];
	XAxiDma AxiDma0;
	int Status;


  init_platform();

	xil_printf("\r\n--- Input array --- \r\n");

	for(i=0;i<N;i++) input[i]=i;
  for(i=0;i<N;i++) xil_printf("%d ",input[i]);

    Xil_DCacheDisable();
    Xil_ICacheDisable();

    // init DMA
    Status = init_dma(&AxiDma0,XPAR_AXIDMA_0_DEVICE_ID);

    xil_printf("DMA initialized\r\n");


    	   xil_printf("Copying data to DMA...\r\n");

    	   //Send data to DMA
    	   Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) input, (N)*sizeof(D_TYPE), XAXIDMA_DMA_TO_DEVICE);
    	   if (Status != XST_SUCCESS) {
    		   xil_printf("Fail!\n") ;
    		   return XST_FAILURE;
    	   }

    	   while ((XAxiDma_Busy(&AxiDma0,XAXIDMA_DMA_TO_DEVICE)));


    	   xil_printf("Receiving data from DMA...\r\n");

    	   //Receive data from DMA
    	   Status = XAxiDma_SimpleTransfer(&AxiDma0,(u32) output, (N)*sizeof(D_TYPE), XAXIDMA_DEVICE_TO_DMA);
    	   if (Status != XST_SUCCESS) {
    		   xil_printf("Fail!\n") ;
    		   return XST_FAILURE;
    	   }

    	   while ((XAxiDma_Busy(&AxiDma0,XAXIDMA_DEVICE_TO_DMA)));

    	   //for(i=0;i<N;i++) printf("%d ",receiveBuf0[i]);
    	   //printf("\n\n");

    	   //xil_printf("\r\n--- Output array --- \r\n");
    	   //for(i=0;i<N;i++) xil_printf("%d ",input[i]);

    	   xil_printf("\r\n--- Comparison --- \r\n");

    	   for(i=0;i<N;i++)
    		   if(input[i]!=output[i]){
    			   printf("%d)\t %d %d -> ",i, input[i],output[i]);
    			   printf("Error!\r\n");
    			   return XST_FAILURE;
    		   }
    	   //xil_printf("Round %d completed\n\r", k+1);
       //}
    	   xil_printf("Finish, no errors\n\r");

    	  // printf("Average times: SW = %f \t HW = %f\n\r",sumSW/TSTEPS,sumHW/TSTEPS);


    return 0;
}
