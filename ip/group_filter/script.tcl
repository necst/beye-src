############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project GROUP_FILTER
set_top group_filter
add_files "group_filter.cpp group_filter.h ../data_structure.cpp input.ppm"
add_files -tb "group_test.cpp"
open_solution "solution1"

#zedboard
set_part {xc7z020clg484-1}


create_clock -period 5 -name default
source "./directives.tcl"
csim_design
csynth_design
#cosim_design
export_design -format ip_catalog
