#include "group_filter.h"

#define CENTER 	          (KERN_SIZE/2)
#define WAIT_TIME         ((CENTER-1+ KERN_SIZE%2)*WIDTH + CENTER-(1-KERN_SIZE%2))
#define START_TIME        ((KERN_SIZE-1)*WIDTH+KERN_SIZE-1)
#define START_FLUSH       (WIDTH*(CENTER+1)-KERN_SIZE+CENTER+1)



hls::LineBuffer<KERN_SIZE,WIDTH,uint8_t> lineBuf;
hls::Window<KERN_SIZE,KERN_SIZE,uint16_t> pixels;

int pixCount = 0;
int outCount = 0;
int flush_pos=0;
int nmax = NMAX;

int count_last = 0;

uint8_t update_last(){	
	if(count_last==nmax-1){
		count_last=0;
		return 1;
	}else{
		count_last+=1;
		return 0;
	}	
}

uint8_t update_last_flush(){
	int last_nmax = update_last();
	int last_flush = (flush_pos==KERN_SIZE*WIDTH-1);
	return last_flush || last_nmax;
}

int last_group = 1;
uint16_t groups[MAX_GROUPS] = {0};
uint16_t counts[MAX_GROUPS] = {0};
int up_values[WIDTH]={0};
int left=0;

uint16_t compute_filter(hls::Window<KERN_SIZE,KERN_SIZE,uint16_t> & window, int x){

	#pragma HLS inline

	int current,up,tmp,index,upgroup,tmp_group,add_to,count;

	current = window.getval(1,1);

	if(current==0){
	
		//gets upper value and group
		up = up_values[x];
		upgroup = groups[up];
		
		int toadd = 0;
		
		if((up==0) && (left==0)){
			//new group
			tmp = last_group;
			groups[last_group]=0;
			last_group++;
			index = tmp;
		}else if(up!=0){
			//conflict between upper and left groups!
			if((left!=0) && (left!=up)){
				
				if(upgroup!=0){
					//up is a small group, links left->up supergroup
					groups[left] = upgroup;
					index=upgroup;
				}else{
					//up is a supergroup, links left->up directly
					groups[left] = up;
					index = up;
				}

				tmp = left;
			}else{
				tmp = up;
				index = up;
			}
		}else{
			tmp=left;
			index = left;
		}

	}else{
		tmp=0;
	}


	tmp_group = groups[tmp];


	//update group's pixels count
	if(!((up==0) && (left==0))){
		if(tmp_group==0)
	   		add_to=tmp;
    	else
    	    add_to = tmp_group;

    	count = counts[add_to];

    	count++;

    	counts[add_to]=count;
    }else{
    	counts[last_group]=1;
    }


	//update left and up values
	left = tmp;

	up_values[x] = tmp;

	return tmp;
}


void group_filter(AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out ){

	
	#pragma HLS ARRAY_PARTITION variable=&pixels.val dim=0
	#pragma HLS ARRAY_PARTITION variable=&lineBuf.val dim=1
	#pragma HLS RESOURCE variable=&lineBuf.val core=RAM_2P_BRAM
	#pragma HLS RESOURCE variable=&groups core=RAM_2P_BRAM
	#pragma HLS RESOURCE variable=&counts core=RAM_2P_BRAM


	MY_TYPE_VAL in,out;
	int pixCount = 0;
	int outCount = 0;
	int xOut=0;

	count_last = 0;
	last_group=1;

	DMA_In >> in;
	nmax = in.data;
	in.last = 1;
	DMA_Out << in;
	
	rows:for(register int y=0; y<HEIGHT; y++){
		cols:for(register int x=0; x<WIDTH;x++){

			#pragma HLS pipeline II=1

			DMA_In >> in;

			//fills the window and insert the value into the linebuffer
			pixels.shift_left();
			insert_col_window:for(int row=1;row<KERN_SIZE;row++){
				#pragma HLS unroll
				pixels.insert(lineBuf.getval(row,x),row-1,KERN_SIZE-1);
			}
			pixels.insert(in.data,KERN_SIZE-1,KERN_SIZE-1);
			
			lineBuf.shift_up(x);
			lineBuf.insert_top(in.data, x);

			//send upper vertical border of the image (same as received)
			if(pixCount<WAIT_TIME){
				out.data = 0;
				//out.data = lineBuf.getval(WAIT_TIME/WIDTH+1,outCount%WIDTH);
			}else if(pixCount>=START_TIME){
				if(y>=KERN_SIZE-1 && x>=KERN_SIZE-1){
					out.data = compute_filter(pixels,x-1);
				}else{
					//border, equal to the current center (border pixel)
					out.data = pixels.getval(KERN_SIZE/2,KERN_SIZE/2);
					left=0;
				}
			}

			if(pixCount>WAIT_TIME-1){
				out.last = update_last();

				if(xOut==WIDTH-1)
					xOut = 0;
				else
					xOut++;

				DMA_Out << out;

				outCount++;
			}

			pixCount++;
		}

	}

	//send lower vertical border of the image (same as received) flushing linebuffer
	//starting point is the last pixel filtered
	flush_buffer:for(flush_pos = START_FLUSH; flush_pos < WIDTH*KERN_SIZE; flush_pos++){
		#pragma HLS pipeline II=1
		//set last bit according to NMAX dma burst size
		out.last=update_last_flush();
		
		out.data = 0;
		outCount++;
		DMA_Out << out;
	}


	int count=0;

	//flushes groups and counts
	for(register int j=0;j<MAX_GROUPS;j++){
		#pragma HLS pipeline II=1

		int other = groups[j];

		if(other==0){
			count = counts[j];
		}else{
			count = counts[other];
		}

		//count = counts[j];

		if(count>THRESHOLD) out.data = 1; //count;
		else out.data = 0; //count;

		//out.data = count;

		out.last = 0;

		if(j==MAX_GROUPS-1) out.last=1;

		DMA_Out << out;

	}






}
	
