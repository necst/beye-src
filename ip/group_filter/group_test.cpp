#include "group_filter.h"

#define RED 0
#define GREEN 1
#define BLUE 2

void print_matrix(short mat[][WIDTH]){
	int i,j;

	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			printf("%3d ",mat[i][j]);
		}
		printf("\n");
	}
}

//read a single channel from image
void load_ppm(char *filename, uint32_t mat[],int rgb){
	int i,j,is_rgb;
	char mode[2];
	uint32_t buf[3],pix;

	FILE *input = fopen(filename,"r");
	if(input==NULL){
		printf("Error in loading image %s\n",filename);
		exit(1);
	}

	fscanf(input,"%s %*d %*d %*d\n",mode);
	if(strcmp(mode,"P2")==0) is_rgb=0;
	else if(strcmp(mode,"P3")==0) is_rgb=1;
	else{
		printf("Image error, mode not present (either P2 or P3)\n");
		exit(1);
	}
	//Caricamento pixels
	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			
			fscanf(input,"%u",&buf[RED]);
			if(is_rgb){
				fscanf(input,"%u",&buf[GREEN]);
				fscanf(input,"%u",&buf[BLUE]);
			}

			if(is_rgb) pix=buf[rgb];
			else pix=buf[RED];

			mat[i*WIDTH+j]=pix;
		}
	}

	fclose(input);
}

void save_ppm(char *filename, uint32_t mat[]){
	int i,j;
	FILE *output = fopen(filename,"w");
	if(output==NULL){
		printf("Error in opening file %s in write mode\n",filename);
		exit(1);
	}

	fprintf(output,"P2 %d %d 255\n",WIDTH,HEIGHT);

	for(int i = 0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			fprintf(output,"%3u ",mat[i*WIDTH+j]);
		}
		fprintf(output,"\n");
	}

	fclose(output);
}


void dump_counts(char *filename,uint32_t mat[],int counts[]){
	FILE *output = fopen(filename,"w");
	if(output==NULL){
		printf("Error in opening file %s in write mode\n",filename);
		exit(1);
	}
	
	//dump image output
	for(int i=0;i<MAX_GROUPS;i++){
		fprintf(output,"%d\n", counts[i]);
	}

	for(int j=0;j<WIDTH*HEIGHT;j++){
		fprintf(output,"%d\n",mat[j]);
	}
}


void copy_matrix(uint32_t src[][WIDTH],uint32_t dst[][WIDTH]){
	for(int i=0;i<HEIGHT;i++){
		for(int j=0;j<WIDTH;j++){
			dst[i][j]=src[i][j];
		}
	}
}


void group_filter_soft(uint32_t img[], uint32_t support[], int groups_map[], int width, int height, int threshold){

	int groups[MAX_GROUPS] = {0};
	int counts[MAX_GROUPS] = {0};
	//static int support[N]={0};
	int y,x,up,left,tmp,a,final;

	int index = 0;

	a=1;
	//grouping and counting loop

	for(y=1;y<height-1;y++){
		for(x=1;x<width-1;x++){

			if(img[val(y,x)]==0){
				up = support[val(y-1,x)];
				left = support[val(y,x-1)];

				if((up==0) && (left==0)){
					tmp = a;
					groups[a]=0;
					a+=1;
				}else if(up!=0){
					if((left!=0) && (left!=up)){
						if(groups[up]!=0){
							groups[left] = groups[up];
							index = groups[up];
						}else{
							groups[left] = up;
							index = up;
						}

						tmp = left;
					}else{
						tmp = up;
						index=up;
					}
				}else{
					tmp=left;
					index = left;
				}

				support[val(y,x)]=tmp;

				//counts[index]++;

				if(groups[support[val(y,x)]]==0)
		            counts[support[val(y,x)]]+=1;
		        else
		            counts[groups[support[val(y,x)]]]+=1;

			}else{
				support[val(y,x)]=0;
			}

			

		}
	}

	int j,count;

	for(j=0;j<MAX_GROUPS;j++){
		int other = groups[j];

		if(other==0){
			count = counts[j];
		}else{
			count = counts[other];
		}

		if(count>threshold) groups_map[j] = 1;
		else groups_map[j] = 0;

		//groups_map[j] = counts[j];

	}


    //thresholding loop
	/*for(y=0;y<height;y++){
		for(x=0;x<width;x++){
			if(groups[support[val(y,x)]]==0)
				final = counts[support[val(y,x)]];
			else
				final = counts[groups[support[val(y,x)]]];

			if(final < threshold)
				img[val(y,x)] = 255;
		}
	}*/
	


}


void threshold(uint32_t img[], int groups_map[], int width, int height, int threshold){


	int y,x;

	for(y=0;y<height;y++){
		for(x=0;x<width;x++){

			if(img[val(y,x)]==0) img[val(y,x)]=255;
			else{
				

				int count = groups_map[img[val(y,x)]];
				if(count==1) img[val(y,x)]=0;
				else img[val(y,x)]=255;
			}

		}
	}

}

int main(){
	int i,j,k,m,ok,lstok,sum,max;

	AXI_STREAM_VAL in;
	AXI_STREAM_VAL out;

	AXI_VAL a,b;

	static uint32_t input[HEIGHT*WIDTH];
	static uint32_t soft[HEIGHT*WIDTH];
	static uint32_t tmp[HEIGHT*WIDTH];
	static uint32_t hard[HEIGHT*WIDTH];
	static uint32_t support[N]={0};

	int group_map_hard[MAX_GROUPS]={0};
	int group_map_soft[MAX_GROUPS]={0};

	int out_hard[HEIGHT*WIDTH]={0};

	int nmax = rand()%NMAX;

	ok = 1;
	lstok=1;

	//RIEMPIE A CASO MATRICE IMMAGINE

	printf("Filling matrix\n");
	/*for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			int caso = rand() % 10;

			if(caso>=5)
				input[i*WIDTH+j]=255;
			else
				input[i*WIDTH+j]=0;
		}
	}*/

	load_ppm("input.ppm",input,GREEN);



	for(int test=0;test <N_TESTS;test++){
		printf("Test %d\n",test+1);


		//reset support matrix
		for(i=0;i<N;i++)
			support[i]=0;

		//print_matrix(prova);
		a.data = nmax;
		in.write(a);

		//Sends data to core input and calls ip core
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				a.data = input[i*WIDTH+j];
				in.write(a);
			}
		}
		group_filter(in,out);
		
		out.read(b);
		if(b.data != nmax){
			lstok=0;
			printf("Error in propagating NMAX parameter");
		}

		printf("Generating outputs\n");
		//Get software output
		
		group_filter_soft(input, support, group_map_soft, WIDTH, HEIGHT, THRESHOLD);

		//Get Hardware output
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				out.read(b);
				hard[i*WIDTH+j]=b.data;
				//Checks proper position of tlast bits (every NMAX integers and at last one)
					if(((i==HEIGHT-1 && j==WIDTH-1) || ((i*WIDTH+j+1)%nmax==0 )) && b.last!=1){
					lstok = 0;
					printf("Error, last not set to 1 at [%d][%d]\n",i,j);
				}
			}
		}


		//reads group_map_hard

		for(j=0;j<MAX_GROUPS;j++){
			out.read(b);
			group_map_hard[j]=b.data;
			if(j==MAX_GROUPS-1 && b.last!=1){
				lstok=0;
				printf("Error in last in groups");
			}
		}

		/*
		printf("Software output\n");
		print_matrix(prova);
		printf("\nHardware output\n");
		printf("\n");print_matrix(output);printf("\n");
		*/


		dump_counts("groups.txt",hard,group_map_hard);

		//threshold(hard,group_map_hard, WIDTH, HEIGHT, THRESHOLD);
		//threshold(support, group_map_soft, WIDTH, HEIGHT, THRESHOLD);

		save_ppm("hardware_out.ppm",hard);
		save_ppm("software_out.ppm",support);

		printf("Comparing...\n");
		//Comparison bewteen SW and HW
		for(i=CENTER;i<HEIGHT-(CENTER-1+KERN_SIZE%2);i++){
			for(j=CENTER;j<WIDTH-(CENTER-1+KERN_SIZE%2);j++){
				if(support[i*WIDTH+j]!=hard[i*WIDTH+j]){
					//printf("\nfailed at [%d][%d] .... %d <> %d\n",i,j,support[i*WIDTH+j],hard[i*WIDTH+j]);
					ok=0;
				}
			}
		}

		//compares group map
		for(j=1;j<MAX_GROUPS;j++){
			if(group_map_soft[j]!=group_map_hard[j]){
				printf("\nfailed at group %d.... %d <> %d\n",j,group_map_soft[j],group_map_hard[j]);
				ok=0;
			}
		}


		//resets arrays


	}

	
	if(ok && lstok){
		printf("Test passed\n");
		return 0;
	}else{
		printf("Test failed\n");
		return 1;
	}

}

