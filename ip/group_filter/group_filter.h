#ifndef GROUP_FILTER
#define GROUP_FILTER


#include <ap_int.h>
#include <hls_stream.h>
#include <hls_video.h>
#include <stdint.h>
#include <stdlib.h>
#include "../parameters.h"
#include "../data_structure.h"

#define KERN_SIZE 3
#define CENTER (KERN_SIZE/2)

#define THRESHOLD 100

#define N (WIDTH*HEIGHT)
#define MAX_GROUPS 10000
#define THRESHOLD 100

#define val(y,x) ((y)*width+(x))


void group_filter( AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out );

#endif
