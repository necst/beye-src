#include "group_delete.h"


int nmax = NMAX;
int outCount = 0;

int count_last = 0;

uint8_t update_last(){	
	if((count_last==nmax-1) || (outCount==WIDTH*HEIGHT-1)) {
		count_last=0;
		return 1;
	}else{
		count_last+=1;
		return 0;
	}	
}

uint16_t counts[MAX_GROUPS] = {0};


void group_delete(AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out ){

	#pragma HLS RESOURCE variable=counts core=RAM_2P_BRAM


	MY_TYPE_VAL in,out;

	int out_val,in_val,count;

	count_last = 0;
	outCount=0;

	DMA_In >> in;
	nmax = in.data;
	in.last = 1;
	DMA_Out << in;


	//load counts

	for(register int k=0;k<MAX_GROUPS;k++){
		
		#pragma HLS pipeline II=1
		
		DMA_In >> in;
		counts[k]=in.data;
	}


	//double cycle to 
	rows:for(register int y=0; y<HEIGHT; y++){
		cols:for(register int x=0; x<WIDTH;x++){

			#pragma HLS pipeline II=1

			DMA_In >> in;

			in_val = in.data;

			if(in_val==0) out_val = 255;
			else{
				count = counts[in_val];
				if(count==1) out_val=0;
				else out_val=255;
			}
			
			out.data = out_val;
			out.last = update_last();

			outCount++;

			DMA_Out << out;
		}

	}

}
