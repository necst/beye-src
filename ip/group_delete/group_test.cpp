#include "group_delete.h"

#define RED 0
#define GREEN 1
#define BLUE 2

void print_matrix(short mat[][WIDTH]){
	int i,j;

	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			printf("%3d ",mat[i][j]);
		}
		printf("\n");
	}
}


//read a single channel from image
void load_ppm(char *filename, uint32_t mat[],int rgb){
	int i,j,is_rgb;
	char mode[2];
	uint32_t buf[3],pix;

	FILE *input = fopen(filename,"r");
	if(input==NULL){
		printf("Error in loading image %s\n",filename);
		exit(1);
	}

	fscanf(input,"%s %*d %*d %*d\n",mode);
	if(strcmp(mode,"P2")==0) is_rgb=0;
	else if(strcmp(mode,"P3")==0) is_rgb=1;
	else{
		printf("Image error, mode not present (either P2 or P3)\n");
		exit(1);
	}
	//Caricamento pixels
	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			
			fscanf(input,"%u",&buf[RED]);
			if(is_rgb){
				fscanf(input,"%u",&buf[GREEN]);
				fscanf(input,"%u",&buf[BLUE]);
			}

			if(is_rgb) pix=buf[rgb];
			else pix=buf[RED];

			mat[i*WIDTH+j]=pix;
		}
	}

	fclose(input);
}

void save_ppm(char *filename, uint32_t mat[]){
	int i,j;
	FILE *output = fopen(filename,"w");
	if(output==NULL){
		printf("Error in opening file %s in write mode\n",filename);
		exit(1);
	}

	fprintf(output,"P2 %d %d 255\n",WIDTH,HEIGHT);

	for(int i = 0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			fprintf(output,"%3u ",mat[i*WIDTH+j]);
		}
		fprintf(output,"\n");
	}

	fclose(output);
}



void load_counts(char *filename,uint32_t mat[],int counts[]){
	FILE *input = fopen(filename,"r");
	if(input==NULL){
		printf("Error in opening file %s in read mode\n",filename);
		exit(1);
	}
	
	//dump image output
	for(int i=0;i<MAX_GROUPS;i++){
		fscanf(input,"%d", &counts[i]);
	}

	for(int j=0;j<WIDTH*HEIGHT;j++){
		fscanf(input,"%d",&mat[j]);
	}
}


void threshold(uint32_t in[],uint32_t out[], int groups_map[], int width, int height, int threshold){


	int y,x;

	for(y=0;y<height;y++){
		for(x=0;x<width;x++){

			if(in[val(y,x)]==0) out[val(y,x)]=255;
			else{
				

				int count = groups_map[in[val(y,x)]];
				if(count==1) out[val(y,x)]=0;
				else out[val(y,x)]=255;
			}

		}
	}

}

int main(){
	int i,j,k,m,ok,lstok,sum,max;

	AXI_STREAM_VAL in;
	AXI_STREAM_VAL out;

	AXI_VAL a,b;


	int groups[MAX_GROUPS]={0};
	uint32_t input[WIDTH*HEIGHT]={0};

	uint32_t out_hard[WIDTH*HEIGHT]={0};
	uint32_t out_soft[WIDTH*HEIGHT]={0};

	int nmax = rand()%NMAX;

	ok = 1;
	lstok=1;

	//RIEMPIE A CASO MATRICE IMMAGINE

	printf("Filling matrix\n");

	load_counts("groups.txt",input,groups);

	for(int test=0;test<N_TESTS;test++){
		printf("Test %d\n",test+1);


		//print_matrix(prova);
		a.data = nmax;
		in.write(a);

		//Sends data to core input and calls ip core
		for(i=0;i<MAX_GROUPS;i++){
			a.data = groups[i];
			in.write(a);
		}

		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				a.data = input[i*WIDTH+j];
				in.write(a);
			}
		}
		group_delete(in,out);
		
		out.read(b);
		if(b.data != nmax){
			lstok=0;
			printf("Error in propagating NMAX parameter");
		}

		printf("Generating outputs\n");
		//Get software output
		


		//Get Hardware output
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				out.read(b);
				out_hard[i*WIDTH+j]=b.data;
				//Checks proper position of tlast bits (every NMAX integers and at last one)
					if(((i==HEIGHT-1 && j==WIDTH-1) || ((i*WIDTH+j+1)%nmax==0 )) && b.last!=1){
					lstok = 0;
					printf("Error, last not set to 1 at [%d][%d]\n",i,j);
				}
			}
		}



		threshold(input, out_soft, groups, WIDTH, HEIGHT, THRESHOLD);

		save_ppm("hardware_out.ppm",out_hard);
		save_ppm("software_out.ppm",out_soft);

		printf("Comparing...\n");
		//Comparison bewteen SW and HW
		for(i=CENTER;i<HEIGHT-(CENTER-1+KERN_SIZE%2);i++){
			for(j=CENTER;j<WIDTH-(CENTER-1+KERN_SIZE%2);j++){
				if(out_soft[i*WIDTH+j]!=out_hard[i*WIDTH+j]){
					printf("\nfailed at [%d][%d] .... %d <> %d\n",i,j,out_soft[i*WIDTH+j],out_hard[i*WIDTH+j]);
					ok=0;
				}
			}
		}

	}

	
	if(ok && lstok){
		printf("Test passed\n");
		return 0;
	}else{
		printf("Test failed\n");
		return 1;
	}

}

