
#include "match_filter.h"
#include "matrices.h"

#define RED 0
#define GREEN 1
#define BLUE 2

void print_matrix(short mat[][WIDTH]){
	int i,j;

	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			printf("%3d ",mat[i][j]);
		}
		printf("\n");
	}
}

//read a single channel from image
void load_ppm(char *filename, uint32_t mat[][WIDTH],int rgb){
	int i,j,is_rgb;
	char mode[2];
	uint32_t buf[3],pix;

	FILE *input = fopen(filename,"r");
	if(input==NULL){
		printf("Error in loading image %s\n",filename);
		exit(1);
	}

	fscanf(input,"%s %*d %*d %*d\n",mode);
	if(strcmp(mode,"P2")==0) is_rgb=0;
	else if(strcmp(mode,"P3")==0) is_rgb=1;
	else{
		printf("Image error, mode not present (either P2 or P3)\n");
		exit(1);
	}
	//Caricamento pixels
	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			
			fscanf(input,"%u",&buf[RED]);
			if(is_rgb){
				fscanf(input,"%u",&buf[GREEN]);
				fscanf(input,"%u",&buf[BLUE]);
			}

			if(is_rgb) pix=buf[rgb];
			else pix=buf[RED];

			mat[i][j]=pix;
		}
	}

	fclose(input);
}

void save_ppm(char *filename, uint32_t mat[][WIDTH]){
	int i,j;
	FILE *output = fopen(filename,"w");
	if(output==NULL){
		printf("Error in opening file %s in write mode\n",filename);
		exit(1);
	}

	fprintf(output,"P2 %d %d 255\n",WIDTH,HEIGHT);

	for(int i = 0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			fprintf(output,"%3u ",mat[i][j]);
		}
		fprintf(output,"\n");
	}

	fclose(output);
}

void copy_matrix(uint32_t src[][WIDTH],uint32_t dst[][WIDTH]){
	for(int i=0;i<HEIGHT;i++){
		for(int j=0;j<WIDTH;j++){
			dst[i][j]=src[i][j];
		}
	}
}

int main(){
	int i,j,k,m,ok,lstok,sum,max;

	AXI_STREAM_VAL in;
	AXI_STREAM_VAL out;

	AXI_VAL a,b;

	static uint32_t input[HEIGHT][WIDTH];
	static uint32_t soft[HEIGHT][WIDTH];
	static uint32_t tmp[HEIGHT][WIDTH];
	static uint32_t hard[HEIGHT][WIDTH];

	int nmax = rand()%NMAX;

	ok = 1;
	lstok=1;

	//RIEMPIE A CASO MATRICE IMMAGINE

	printf("Filling matrix\n");
	/*for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			prova[i][j]=rand() % 255;
		}
	}*/

	load_ppm("input.ppm",input,GREEN);



	for(int test=0;test <N_TESTS;test++){
		printf("Test %d\n",test+1);


		//print_matrix(prova);
		a.data = nmax;
		in.write(a);

		//Sends data to core input and calls ip core
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				a.data = input[i][j];
				in.write(a);
			}
		}
		match_filter(in,out);
		
		out.read(b);
		if(b.data != nmax){
			lstok=0;
			printf("Error in propagating NMAX parameter");
		}

		printf("Generating outputs\n");
		//Get software output
		
		for(i=CENTER;i<HEIGHT-(CENTER-1+KERN_SIZE%2);i++){
			for(j=CENTER;j<WIDTH-(CENTER-1+KERN_SIZE%2);j++){
				
				tmp[i][j]=0;
				max=0;
				for(int theta=0;theta<NUM_FILTERS;theta++){
					sum=0;
					for(int row = 0; row<KERN_SIZE;row++){
						for(int col=0;col<KERN_SIZE;col++){
							sum+=((int)coeffs[theta][row][col] * (int)input[i-CENTER+row][j-CENTER+col]);
						}
					}
					if(sum>max) max =sum;
				}

				if(max/DIVISOR<255)
					tmp[i][j]=255-max/DIVISOR;
				else if(max/DIVISOR>=255)
					tmp[i][j]=0;
				else 
					tmp[i][j]=255;
			}	
		}
		for(i=CENTER;i<HEIGHT-(CENTER-1+KERN_SIZE%2);i++){
			for(j=CENTER;j<WIDTH-(CENTER-1+KERN_SIZE%2);j++){
				soft[i][j]=(short)tmp[i][j];
			}
		}

		//Get Hardware output
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				out.read(b);
				hard[i][j]=b.data;
				//Checks proper position of tlast bits (every NMAX integers and at last one)
				if(((i==HEIGHT-1 && j==WIDTH-1) || ((i*WIDTH+j+1)%nmax==0 )) && b.last!=1){
					lstok = 0;
					printf("Error, last not set to 1 at [%d][%d]\n",i,j);
				}
			}
		}
		/*
		printf("Software output\n");
		print_matrix(prova);
		printf("\nHardware output\n");
		printf("\n");print_matrix(output);printf("\n");
		*/

		save_ppm("hardware_out.ppm",hard);
		save_ppm("software_out.ppm",soft);

		printf("Comparing...\n");
		//Comparison bewteen SW and HW
		for(i=CENTER;i<HEIGHT-(CENTER-1+KERN_SIZE%2);i++){
			for(j=CENTER;j<WIDTH-(CENTER-1+KERN_SIZE%2);j++){
				if(soft[i][j]!=hard[i][j]){
					printf("\nfailed at [%d][%d] .... %d <> %d\n",i,j,soft[i][j],hard[i][j]);
					ok=0;
				}
			}
		}
	}

	
	if(ok && lstok){
		printf("Test passed\n");
		return 0;
	}else{
		printf("Test failed\n");
		return 1;
	}

}

