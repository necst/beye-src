############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project MATCH_FILTER
set_top match_filter
add_files "match_filter.cpp match_filter.h matrices.h ../data_structure.cpp input.ppm"
add_files -tb "match_test.cpp"
open_solution "solution1"

#zedboard
set_part {xc7z020clg484-1}


create_clock -period 6 -name default
source "./directives.tcl"
csim_design
csynth_design
#cosim_design
export_design -format ip_catalog
