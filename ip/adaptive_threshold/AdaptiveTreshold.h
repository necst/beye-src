#ifndef FILTER_LIB
#define FILTER_LIB

//#define AP_INT_MAX_W 4096
#include <ap_int.h>
#include <hls_stream.h>
#include <hls_video.h>
#include <stdint.h>
#include <stdlib.h>
#include "../parameters.h"
#include "../data_structure.h"

#define KERN_SIZE 9
#define CENTER (KERN_SIZE/2)
#define MEAN_C 3

//time used to fill the buffer
#define WAIT_TIME         (CENTER*WIDTH+CENTER)

//time at which the pixels start to be filtered and sent to output
#define START_TIME        ((KERN_SIZE-1)*WIDTH+KERN_SIZE-1)

//starting position in the buffer to be flushed
#define START_FLUSH       (WIDTH*(CENTER+1)-KERN_SIZE+CENTER+1)

/* Main core function prototype */
void adapt_tresh(AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out );

#endif
