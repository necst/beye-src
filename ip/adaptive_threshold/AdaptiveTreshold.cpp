#include "AdaptiveTreshold.h"

/*
	LineBuffer used to store KERN_SIZE lines.
	The buffer is filled while streaming the input pixels
*/
hls::LineBuffer<KERN_SIZE,WIDTH,uint8_t> lineBuf;

/*
	Window on which the filter is computed
*/
hls::Window<KERN_SIZE,KERN_SIZE,uint8_t> pixels;

int pixCount = 0;	//Number of the current input pixel
int outCount = 0;	//Number of the current output pixel

static int count_last = 0; //current count of output pixels


int nmax = NMAX;

/*
	Used to update the last condition, that is the value
	to be given to "last" flag of AxiStream protocol, for
	the output value.

	@returns  0 or 1
*/
uint8_t update_last(){

	//resets the count and return the correct last value
	if(count_last==nmax-1){
		count_last=0;
		return 1;
	}else{
		count_last++;
		return 0;
	}
}


/*
	Used to update the "last" condition, like update_last() function,
	but considering also the final pixel of the output image as last,
	in order to have a correct HW/SW alignment.

	@returns  0 or 1
*/
uint8_t update_last_flush(){
	int last_nmax = update_last();
	int last_flush = (outCount==WIDTH*HEIGHT-1);
	return last_flush || last_nmax;
}

/*
	Function used to compute the filter from the given window
	The filter consists of a convolution operation between the window
	and the coefficents matrices.

	@param window     The window on which compute the convolution filter

	@returns  The filter obtained value, as a 8 bit unsigned integer (pixel value)
*/

int mean(hls::Window<KERN_SIZE,KERN_SIZE,uint8_t> & window){
	int area = 0;
	int mean = 0;

	#pragma HLS INLINE

		for(int row=0;row<KERN_SIZE;row++)
			for(int col=0;col<KERN_SIZE;col++)
				area += window.getval(row, col);

		mean = area/(KERN_SIZE * KERN_SIZE);

	return mean;
}

uint8_t compute_adap_tresh(hls::Window<KERN_SIZE,KERN_SIZE,uint8_t> & window){

		#pragma HLS INLINE

		int th = mean(pixels)-MEAN_C;

			if (window.getval(CENTER, CENTER) >= th)
				return 255;
			else return 0;
}


/*
	Main function of the core, perform the filter for each pixel of the image,
	managing also linebuffer, window and output flags.


*/

void adapt_tresh(AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out ){

	//partition the window into single registers
	#pragma HLS ARRAY_PARTITION variable=&pixels.val dim=0

	//partition the line buffer into single lines, in order to assist the pipeline creation
	#pragma HLS ARRAY_PARTITION variable=&lineBuf.val dim=1

	//tells vivado hls to use 2 ports BRAMs, to reduce BRAM usage
	#pragma HLS RESOURCE variable=&lineBuf.val core=RAM_2P_BRAM


	count_last=0;
	pixCount=0;
	outCount=0;

	MY_TYPE_VAL in,out;

	DMA_In >> in;
	nmax = in.data;
	in.last = 1;
	DMA_Out << in;


	rows:for(register int y=0; y<HEIGHT; y++){
		cols:for(register int x=0; x<WIDTH;x++){

			#pragma HLS pipeline II=1

			DMA_In >> in;


			//Update the filter window, by shifting it to the left
			// and updating the last column with values from linebuffer
			pixels.shift_left();
			insert_col_window:for(int row=1;row<KERN_SIZE;row++){
				#pragma HLS unroll
				pixels.insert(lineBuf.getval(row,x),row-1,KERN_SIZE-1);
			}
			//the newly received pixel is inserted outside the linebuffer
			// to improve the pipelining performance (read before write approach)
			pixels.insert(in.data,KERN_SIZE-1,KERN_SIZE-1);

			//insert the input pixel into linebuffer
			lineBuf.shift_up(x);
			lineBuf.insert_top(in.data, x);



			if(pixCount<WAIT_TIME){
				//sends upper border that won't be processed, waiting for the buffer to be filled
				out.data = in.data;

			}else if(pixCount>=START_TIME){
				if(y>=KERN_SIZE-1 && x>=KERN_SIZE-1){
					//internal pixel that is filtered and sent
					out.data = (uint8_t) compute_adap_tresh(pixels);
				}else{
					//border pixels are sent without filtering
					out.data = pixels.getval(CENTER,CENTER);
				}
			}

			//sends the filtered pixel in output
			if(pixCount<WAIT_TIME || pixCount>=START_TIME){
				out.last = update_last();

				DMA_Out << out;

				outCount++;
			}

			pixCount++;
		}

	}

	//Flushes the buffer by sending the last unfiltered rows to output
	//"pos" indicates a counter used for the current position in the buffer
	flush_buffer:for(int pos = START_FLUSH; pos < WIDTH*KERN_SIZE; pos++){
		#pragma HLS pipeline II=1
		//set last bit according to NMAX dma burst size
		out.last = update_last_flush();
		out.data = lineBuf.getval(pos/WIDTH, pos%WIDTH);
		outCount++;
		DMA_Out << out;
	}


}
