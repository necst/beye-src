<AutoPilot:project xmlns:AutoPilot="com.autoesl.autopilot.project" projectType="C/C++" name="ADAPTIVETRESHOLD" top="adapt_tresh">
    <Simulation argv="">
        <SimFlow name="csim" setup="false" optimizeCompile="false" clean="false" ldflags="" mflags=""/>
    </Simulation>
    <files>
        <file name="../../AdaptiveTreshold-test.cpp" sc="0" tb="1" cflags=" "/>
        <file name="AdaptiveTreshold.cpp" sc="0" tb="false" cflags=""/>
        <file name="AdaptiveTreshold.h" sc="0" tb="false" cflags=""/>
        <file name="../data_structure.cpp" sc="0" tb="false" cflags=""/>
        <file name="input.ppm" sc="0" tb="false" cflags=""/>
    </files>
    <solutions>
        <solution name="solution1" status=""/>
    </solutions>
</AutoPilot:project>

