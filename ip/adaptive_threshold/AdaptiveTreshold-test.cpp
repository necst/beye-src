#include "AdaptiveTreshold.h"

void copy_matrix(short in[][WIDTH],short out[][WIDTH]){
	for(int i=0;i<HEIGHT;i++)
		for(int j=0;j<WIDTH;j++)
			out[i][j]=in[i][j];
}

void print_matrix(short mat[][WIDTH]){
	int i,j;

	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			printf("%3d ",mat[i][j]);
		}
		printf("\n");
	}
}

int mean(short int k[][WIDTH], int i, int j){

	int area = 0;
	int mean = 0;

	for(int row=0;row<KERN_SIZE;row++)
		for(int col=0;col<KERN_SIZE;col++)
			area += k[i-CENTER+row][j-CENTER+col];

	mean = area/(KERN_SIZE * KERN_SIZE);

	return mean;
}

int main(){
	int i,j,k,m,ok,lstok;

	AXI_STREAM_VAL in;
	AXI_STREAM_VAL out;

	AXI_VAL a,b;

	static short input[HEIGHT][WIDTH];
	static short output_soft[HEIGHT][WIDTH];
	static short output_hard[HEIGHT][WIDTH];
	static int tmp[HEIGHT][WIDTH];

	
	int nmax;
	
	ok = 1;
	lstok=1;

	//Randomly fills input matrix

	printf("Filling matrix\n");
	for(i=0;i<HEIGHT;i++){
		for(j=0;j<WIDTH;j++){
			input[i][j]=rand() % 255;
		}
	}

	//run N_TESTS tests
	for(int test=0;test<N_TESTS;test++){
		printf("Test %d\n",test+1);

		nmax = rand()%NMAX;
		a.data = nmax;
		in.write(a);
	
		//sends data to core input and calls ip core
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				a.data = input[i][j];
				in.write(a);
			}
		}

		//Main core function is called on the two streams
		adapt_tresh(in,out);
		
		out.read(b);
		if(b.data != nmax){
			lstok=0;
			printf("Error in propagating NMAX parameter");
		}

		printf("Generating software output\n");
		
		//Get software output
		copy_matrix(input,output_soft);

		int th=0;

		for(i=CENTER;i<HEIGHT-KERN_SIZE+CENTER+1;i++){
			for(j=CENTER;j<WIDTH-KERN_SIZE+CENTER+1;j++){

				tmp[i][j]=0;

				th = mean(input, i, j)-3;
				if (input[i][j] >= th)
					tmp[i][j]=255;
			}
		}
		for(i=CENTER;i<HEIGHT-KERN_SIZE+CENTER+1;i++){
			for(j=CENTER;j<WIDTH-KERN_SIZE+CENTER+1;j++){
				output_soft[i][j]=(short)tmp[i][j];
			}
		}

		//Get Hardware output
		printf("Generating hardware output\n");
		for(i=0;i<HEIGHT;i++){
			for(j=0;j<WIDTH;j++){
				out.read(b);
				output_hard[i][j]=b.data;

				//Checks proper position of tlast bits (every NMAX integers and at last one)
				if(((i==HEIGHT-1 && j==WIDTH-1) || ((i*WIDTH+j+1)%NMAX==0 )) && b.last!=1){
					lstok = 0;
					printf("Error, last not set to 1 at [%d][%d]\n",i,j);
				}
			}
		}

		#ifdef DEBUG_PRINT
			//Prints input and SW/HW output matrices, for debugging reasons
			printf("Input matrix\n");
			print_matrix(input);
			printf("\nSoftware output\n");
			print_matrix(output_soft);
			printf("\nHardware output\n");
			print_matrix(output_hard);
			printf("\n");
		#endif
		

		printf("Comparing...\n");
		//Comparison bewteen SW and HW
		for(i=0;i<HEIGHT && ok;i++){
			for(j=0;j<WIDTH && ok;j++){
				if(output_soft[i][j]!=output_hard[i][j]){
					printf("\nfailed at [%d][%d] .... %d <> %d\n",i,j,output_soft[i][j],output_hard[i][j]);
					ok=0;
				}
			}
		}
	}


	if(ok && lstok){
		printf("Test passed\n");
		return 0;
	}else{
		printf("Test failed\n");
		return 1;
	}

}
