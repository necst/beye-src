
################################################################
# This is a generated script based on design: system
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2015.3
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   puts "ERROR: This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source system_script.tcl

# If you do not already have a project created,
# you can create a project using the following command:
#    create_project project_1 myproj -part xc7z020clg484-1
#    set_property BOARD_PART em.avnet.com:zed:part0:1.3 [current_project]

# CHECKING IF PROJECT EXISTS
if { [get_projects -quiet] eq "" } {
   puts "ERROR: Please open or create a project!"
   return 1
}



# CHANGE DESIGN NAME HERE
set design_name system

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "ERROR: Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      puts "INFO: Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   puts "INFO: Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   puts "INFO: Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   puts "INFO: Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

puts "INFO: Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   puts $errMsg
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]
  set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]

  # Create ports

  # Create instance: adapt_tresh_0, and set properties
  set adapt_tresh_0 [ create_bd_cell -type ip -vlnv xilinx.com:hls:adapt_tresh:1.0 adapt_tresh_0 ]

  # Create instance: adapt_tresh_1, and set properties
  set adapt_tresh_1 [ create_bd_cell -type ip -vlnv xilinx.com:hls:adapt_tresh:1.0 adapt_tresh_1 ]

  # Create instance: axi_dma, and set properties
  set axi_dma [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma ]
  set_property -dict [ list \
CONFIG.c_include_mm2s_dre {1} \
CONFIG.c_include_s2mm_dre {1} \
CONFIG.c_include_sg {0} \
CONFIG.c_m_axi_mm2s_data_width {64} \
CONFIG.c_m_axis_mm2s_tdata_width {32} \
CONFIG.c_mm2s_burst_size {256} \
CONFIG.c_s2mm_burst_size {256} \
CONFIG.c_sg_include_stscntrl_strm {0} \
CONFIG.c_sg_length_width {23} \
 ] $axi_dma

  # Create instance: axi_dma_1, and set properties
  set axi_dma_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_1 ]
  set_property -dict [ list \
CONFIG.c_include_mm2s_dre {1} \
CONFIG.c_include_s2mm_dre {1} \
CONFIG.c_include_sg {0} \
CONFIG.c_m_axi_mm2s_data_width {64} \
CONFIG.c_m_axis_mm2s_tdata_width {32} \
CONFIG.c_mm2s_burst_size {256} \
CONFIG.c_s2mm_burst_size {256} \
CONFIG.c_sg_include_stscntrl_strm {0} \
CONFIG.c_sg_length_width {23} \
 ] $axi_dma_1

  # Create instance: axi_mem_intercon, and set properties
  set axi_mem_intercon [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_mem_intercon ]
  set_property -dict [ list \
CONFIG.NUM_MI {1} \
CONFIG.NUM_SI {2} \
 ] $axi_mem_intercon

  # Create instance: axi_mem_intercon_1, and set properties
  set axi_mem_intercon_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_mem_intercon_1 ]
  set_property -dict [ list \
CONFIG.NUM_MI {1} \
CONFIG.NUM_SI {2} \
 ] $axi_mem_intercon_1

  # Create instance: circular_bound_0, and set properties
  set circular_bound_0 [ create_bd_cell -type ip -vlnv xilinx.com:hls:circular_bound:1.0 circular_bound_0 ]

  # Create instance: gauss_0, and set properties
  set gauss_0 [ create_bd_cell -type ip -vlnv xilinx.com:hls:gauss:1.0 gauss_0 ]

  # Create instance: group_delete_0, and set properties
  set group_delete_0 [ create_bd_cell -type ip -vlnv xilinx.com:hls:group_delete:1.0 group_delete_0 ]

  # Create instance: group_filter_0, and set properties
  set group_filter_0 [ create_bd_cell -type ip -vlnv xilinx.com:hls:group_filter:1.0 group_filter_0 ]

  # Create instance: match_filter_0, and set properties
  set match_filter_0 [ create_bd_cell -type ip -vlnv xilinx.com:hls:match_filter:1.0 match_filter_0 ]

  # Create instance: median_filter_0, and set properties
  set median_filter_0 [ create_bd_cell -type ip -vlnv xilinx.com:hls:median_filter:1.0 median_filter_0 ]

  # Create instance: median_filter_1, and set properties
  set median_filter_1 [ create_bd_cell -type ip -vlnv xilinx.com:hls:median_filter:1.0 median_filter_1 ]

  # Create instance: processing_system7_0, and set properties
  set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
  set_property -dict [ list \
CONFIG.PCW_USE_S_AXI_HP0 {1} \
CONFIG.PCW_USE_S_AXI_HP1 {1} \
CONFIG.preset {ZedBoard} \
 ] $processing_system7_0

  # Create instance: processing_system7_0_axi_periph, and set properties
  set processing_system7_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 processing_system7_0_axi_periph ]
  set_property -dict [ list \
CONFIG.NUM_MI {2} \
 ] $processing_system7_0_axi_periph

  # Create instance: rst_processing_system7_0_100M, and set properties
  set rst_processing_system7_0_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_processing_system7_0_100M ]

  # Create interface connections
  connect_bd_intf_net -intf_net adapt_tresh_0_DMA_Out [get_bd_intf_pins adapt_tresh_0/DMA_Out] [get_bd_intf_pins group_filter_0/DMA_In]
  connect_bd_intf_net -intf_net adapt_tresh_1_DMA_Out [get_bd_intf_pins adapt_tresh_1/DMA_Out] [get_bd_intf_pins median_filter_1/DMA_In]
  connect_bd_intf_net -intf_net axi_dma_1_M_AXIS_MM2S [get_bd_intf_pins axi_dma_1/M_AXIS_MM2S] [get_bd_intf_pins group_delete_0/DMA_In]
  connect_bd_intf_net -intf_net axi_dma_1_M_AXI_MM2S [get_bd_intf_pins axi_dma_1/M_AXI_MM2S] [get_bd_intf_pins axi_mem_intercon_1/S01_AXI]
  connect_bd_intf_net -intf_net axi_dma_1_M_AXI_S2MM [get_bd_intf_pins axi_dma_1/M_AXI_S2MM] [get_bd_intf_pins axi_mem_intercon_1/S00_AXI]
  connect_bd_intf_net -intf_net axi_dma_M_AXIS_MM2S [get_bd_intf_pins axi_dma/M_AXIS_MM2S] [get_bd_intf_pins median_filter_0/DMA_In]
  connect_bd_intf_net -intf_net axi_dma_M_AXI_MM2S [get_bd_intf_pins axi_dma/M_AXI_MM2S] [get_bd_intf_pins axi_mem_intercon/S01_AXI]
  connect_bd_intf_net -intf_net axi_dma_M_AXI_S2MM [get_bd_intf_pins axi_dma/M_AXI_S2MM] [get_bd_intf_pins axi_mem_intercon/S00_AXI]
  connect_bd_intf_net -intf_net axi_mem_intercon_1_M00_AXI [get_bd_intf_pins axi_mem_intercon_1/M00_AXI] [get_bd_intf_pins processing_system7_0/S_AXI_HP1]
  connect_bd_intf_net -intf_net axi_mem_intercon_M00_AXI [get_bd_intf_pins axi_mem_intercon/M00_AXI] [get_bd_intf_pins processing_system7_0/S_AXI_HP0]
  connect_bd_intf_net -intf_net circular_bound_0_DMA_Out [get_bd_intf_pins axi_dma_1/S_AXIS_S2MM] [get_bd_intf_pins circular_bound_0/DMA_Out]
  connect_bd_intf_net -intf_net gauss_0_DMA_Out [get_bd_intf_pins adapt_tresh_0/DMA_In] [get_bd_intf_pins gauss_0/DMA_Out]
  connect_bd_intf_net -intf_net group_delete_0_DMA_Out [get_bd_intf_pins adapt_tresh_1/DMA_In] [get_bd_intf_pins group_delete_0/DMA_Out]
  connect_bd_intf_net -intf_net group_filter_0_DMA_Out [get_bd_intf_pins axi_dma/S_AXIS_S2MM] [get_bd_intf_pins group_filter_0/DMA_Out]
  connect_bd_intf_net -intf_net match_filter_0_DMA_Out [get_bd_intf_pins gauss_0/DMA_In] [get_bd_intf_pins match_filter_0/DMA_Out]
  connect_bd_intf_net -intf_net median_filter_0_DMA_Out [get_bd_intf_pins match_filter_0/DMA_In] [get_bd_intf_pins median_filter_0/DMA_Out]
  connect_bd_intf_net -intf_net median_filter_1_DMA_Out [get_bd_intf_pins circular_bound_0/DMA_In] [get_bd_intf_pins median_filter_1/DMA_Out]
  connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins processing_system7_0/DDR]
  connect_bd_intf_net -intf_net processing_system7_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins processing_system7_0/FIXED_IO]
  connect_bd_intf_net -intf_net processing_system7_0_M_AXI_GP0 [get_bd_intf_pins processing_system7_0/M_AXI_GP0] [get_bd_intf_pins processing_system7_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_axi_periph_M00_AXI [get_bd_intf_pins axi_dma/S_AXI_LITE] [get_bd_intf_pins processing_system7_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_axi_periph_M01_AXI [get_bd_intf_pins axi_dma_1/S_AXI_LITE] [get_bd_intf_pins processing_system7_0_axi_periph/M01_AXI]

  # Create port connections
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins adapt_tresh_0/aclk] [get_bd_pins adapt_tresh_1/aclk] [get_bd_pins axi_dma/m_axi_mm2s_aclk] [get_bd_pins axi_dma/m_axi_s2mm_aclk] [get_bd_pins axi_dma/s_axi_lite_aclk] [get_bd_pins axi_dma_1/m_axi_mm2s_aclk] [get_bd_pins axi_dma_1/m_axi_s2mm_aclk] [get_bd_pins axi_dma_1/s_axi_lite_aclk] [get_bd_pins axi_mem_intercon/ACLK] [get_bd_pins axi_mem_intercon/M00_ACLK] [get_bd_pins axi_mem_intercon/S00_ACLK] [get_bd_pins axi_mem_intercon/S01_ACLK] [get_bd_pins axi_mem_intercon_1/ACLK] [get_bd_pins axi_mem_intercon_1/M00_ACLK] [get_bd_pins axi_mem_intercon_1/S00_ACLK] [get_bd_pins axi_mem_intercon_1/S01_ACLK] [get_bd_pins circular_bound_0/aclk] [get_bd_pins gauss_0/aclk] [get_bd_pins group_delete_0/aclk] [get_bd_pins group_filter_0/aclk] [get_bd_pins match_filter_0/aclk] [get_bd_pins median_filter_0/aclk] [get_bd_pins median_filter_1/aclk] [get_bd_pins processing_system7_0/FCLK_CLK0] [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0/S_AXI_HP0_ACLK] [get_bd_pins processing_system7_0/S_AXI_HP1_ACLK] [get_bd_pins processing_system7_0_axi_periph/ACLK] [get_bd_pins processing_system7_0_axi_periph/M00_ACLK] [get_bd_pins processing_system7_0_axi_periph/M01_ACLK] [get_bd_pins processing_system7_0_axi_periph/S00_ACLK] [get_bd_pins rst_processing_system7_0_100M/slowest_sync_clk]
  connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins rst_processing_system7_0_100M/ext_reset_in]
  connect_bd_net -net rst_processing_system7_0_100M_interconnect_aresetn [get_bd_pins axi_mem_intercon/ARESETN] [get_bd_pins axi_mem_intercon_1/ARESETN] [get_bd_pins processing_system7_0_axi_periph/ARESETN] [get_bd_pins rst_processing_system7_0_100M/interconnect_aresetn]
  connect_bd_net -net rst_processing_system7_0_100M_peripheral_aresetn [get_bd_pins adapt_tresh_0/aresetn] [get_bd_pins adapt_tresh_1/aresetn] [get_bd_pins axi_dma/axi_resetn] [get_bd_pins axi_dma_1/axi_resetn] [get_bd_pins axi_mem_intercon/M00_ARESETN] [get_bd_pins axi_mem_intercon/S00_ARESETN] [get_bd_pins axi_mem_intercon/S01_ARESETN] [get_bd_pins axi_mem_intercon_1/M00_ARESETN] [get_bd_pins axi_mem_intercon_1/S00_ARESETN] [get_bd_pins axi_mem_intercon_1/S01_ARESETN] [get_bd_pins circular_bound_0/aresetn] [get_bd_pins gauss_0/aresetn] [get_bd_pins group_delete_0/aresetn] [get_bd_pins group_filter_0/aresetn] [get_bd_pins match_filter_0/aresetn] [get_bd_pins median_filter_0/aresetn] [get_bd_pins median_filter_1/aresetn] [get_bd_pins processing_system7_0_axi_periph/M00_ARESETN] [get_bd_pins processing_system7_0_axi_periph/M01_ARESETN] [get_bd_pins processing_system7_0_axi_periph/S00_ARESETN] [get_bd_pins rst_processing_system7_0_100M/peripheral_aresetn]

  # Create address segments
  create_bd_addr_seg -range 0x20000000 -offset 0x0 [get_bd_addr_spaces axi_dma/Data_MM2S] [get_bd_addr_segs processing_system7_0/S_AXI_HP0/HP0_DDR_LOWOCM] SEG_processing_system7_0_HP0_DDR_LOWOCM
  create_bd_addr_seg -range 0x20000000 -offset 0x0 [get_bd_addr_spaces axi_dma/Data_S2MM] [get_bd_addr_segs processing_system7_0/S_AXI_HP0/HP0_DDR_LOWOCM] SEG_processing_system7_0_HP0_DDR_LOWOCM
  create_bd_addr_seg -range 0x20000000 -offset 0x0 [get_bd_addr_spaces axi_dma_1/Data_MM2S] [get_bd_addr_segs processing_system7_0/S_AXI_HP1/HP1_DDR_LOWOCM] SEG_processing_system7_0_HP1_DDR_LOWOCM
  create_bd_addr_seg -range 0x20000000 -offset 0x0 [get_bd_addr_spaces axi_dma_1/Data_S2MM] [get_bd_addr_segs processing_system7_0/S_AXI_HP1/HP1_DDR_LOWOCM] SEG_processing_system7_0_HP1_DDR_LOWOCM
  create_bd_addr_seg -range 0x10000 -offset 0x40410000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_dma_1/S_AXI_LITE/Reg] SEG_axi_dma_1_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x40400000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_dma/S_AXI_LITE/Reg] SEG_axi_dma_Reg

  # Perform GUI Layout
  regenerate_bd_layout -layout_string {
   guistr: "# # String gsaved with Nlview 6.5.5  2015-06-26 bk=1.3371 VDI=38 GEI=35 GUI=JA:1.8
#  -string -flagsOSRD
preplace port DDR -pg 1 -y 340 -defaultsOSRD
preplace port FIXED_IO -pg 1 -y 360 -defaultsOSRD
preplace inst group_filter_0 -pg 1 -lvl 6 -y 620 -defaultsOSRD
preplace inst axi_dma -pg 1 -lvl 7 -y 180 -defaultsOSRD
preplace inst gauss_0 -pg 1 -lvl 4 -y 620 -defaultsOSRD
preplace inst adapt_tresh_0 -pg 1 -lvl 5 -y 620 -defaultsOSRD
preplace inst adapt_tresh_1 -pg 1 -lvl 4 -y 390 -defaultsOSRD
preplace inst axi_mem_intercon_1 -pg 1 -lvl 8 -y 440 -defaultsOSRD
preplace inst axi_dma_1 -pg 1 -lvl 7 -y 410 -defaultsOSRD
preplace inst rst_processing_system7_0_100M -pg 1 -lvl 1 -y 780 -defaultsOSRD
preplace inst match_filter_0 -pg 1 -lvl 3 -y 620 -defaultsOSRD
preplace inst median_filter_0 -pg 1 -lvl 2 -y 610 -defaultsOSRD
preplace inst group_delete_0 -pg 1 -lvl 3 -y 370 -defaultsOSRD
preplace inst median_filter_1 -pg 1 -lvl 5 -y 410 -defaultsOSRD
preplace inst axi_mem_intercon -pg 1 -lvl 8 -y 140 -defaultsOSRD
preplace inst circular_bound_0 -pg 1 -lvl 6 -y 430 -defaultsOSRD
preplace inst processing_system7_0_axi_periph -pg 1 -lvl 6 -y 150 -defaultsOSRD
preplace inst processing_system7_0 -pg 1 -lvl 9 -y 420 -defaultsOSRD
preplace netloc processing_system7_0_DDR 1 9 1 NJ
preplace netloc adapt_tresh_1_DMA_Out 1 4 1 N
preplace netloc axi_dma_1_M_AXI_S2MM 1 7 1 2410
preplace netloc circular_bound_0_DMA_Out 1 6 1 1990
preplace netloc median_filter_0_DMA_Out 1 2 1 700
preplace netloc processing_system7_0_axi_periph_M00_AXI 1 6 1 1940
preplace netloc axi_dma_1_M_AXI_MM2S 1 7 1 2390
preplace netloc axi_mem_intercon_1_M00_AXI 1 8 1 2710
preplace netloc axi_dma_1_M_AXIS_MM2S 1 2 6 700 10 NJ 10 NJ 10 NJ 10 NJ 10 2340
preplace netloc processing_system7_0_M_AXI_GP0 1 5 5 1630 300 NJ 300 NJ 290 NJ 290 3170
preplace netloc gauss_0_DMA_Out 1 4 1 1290
preplace netloc processing_system7_0_FCLK_RESET0_N 1 0 10 20 510 NJ 510 NJ 510 NJ 510 NJ 510 NJ 510 NJ 520 NJ 600 NJ 600 3160
preplace netloc group_delete_0_DMA_Out 1 3 1 N
preplace netloc axi_mem_intercon_M00_AXI 1 8 1 2710
preplace netloc rst_processing_system7_0_100M_peripheral_aresetn 1 1 7 380 520 690 300 980 320 1280 320 1580 330 1950 40 2400
preplace netloc adapt_tresh_0_DMA_Out 1 5 1 1590
preplace netloc processing_system7_0_FIXED_IO 1 9 1 NJ
preplace netloc match_filter_0_DMA_Out 1 3 1 990
preplace netloc axi_dma_M_AXI_MM2S 1 7 1 2370
preplace netloc axi_dma_M_AXI_S2MM 1 7 1 2360
preplace netloc axi_dma_M_AXIS_MM2S 1 1 7 400 270 NJ 270 NJ 270 NJ 270 NJ 290 NJ 290 2330
preplace netloc rst_processing_system7_0_100M_interconnect_aresetn 1 1 7 NJ 280 NJ 280 NJ 280 NJ 280 1620 310 NJ 70 2350
preplace netloc processing_system7_0_FCLK_CLK0 1 0 10 30 610 390 540 680 290 1000 310 1300 310 1600 320 1970 60 2380 590 2720 550 3170
preplace netloc median_filter_1_DMA_Out 1 5 1 N
preplace netloc group_filter_0_DMA_Out 1 6 1 1980
preplace netloc processing_system7_0_axi_periph_M01_AXI 1 6 1 1960
levelinfo -pg 1 0 200 540 840 1140 1440 1780 2160 2560 2940 3190 -top 0 -bot 870
",
}

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


