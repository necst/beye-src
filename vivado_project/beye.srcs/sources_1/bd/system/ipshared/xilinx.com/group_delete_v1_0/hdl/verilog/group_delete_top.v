// ==============================================================
// File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.3
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ==============================================================

`timescale 1 ns / 1 ps
module group_delete_top (
DMA_In_TVALID,
DMA_In_TREADY,
DMA_In_TDATA,
DMA_In_TLAST,
DMA_Out_TVALID,
DMA_Out_TREADY,
DMA_Out_TDATA,
DMA_Out_TLAST,
aresetn,
aclk
);

parameter RESET_ACTIVE_LOW = 1;

input DMA_In_TVALID ;
output DMA_In_TREADY ;
input [32 - 1:0] DMA_In_TDATA ;
input [1 - 1:0] DMA_In_TLAST ;


output DMA_Out_TVALID ;
input DMA_Out_TREADY ;
output [32 - 1:0] DMA_Out_TDATA ;
output [1 - 1:0] DMA_Out_TLAST ;

input aresetn ;

input aclk ;


wire DMA_In_TVALID;
wire DMA_In_TREADY;
wire [32 - 1:0] DMA_In_TDATA;
wire [1 - 1:0] DMA_In_TLAST;


wire DMA_Out_TVALID;
wire DMA_Out_TREADY;
wire [32 - 1:0] DMA_Out_TDATA;
wire [1 - 1:0] DMA_Out_TLAST;

wire aresetn;


wire [32 - 1:0] sig_group_delete_DMA_In_V_data_V_dout;
wire sig_group_delete_DMA_In_V_data_V_empty_n;
wire sig_group_delete_DMA_In_V_data_V_read;
wire [1 - 1:0] sig_group_delete_DMA_In_V_last_V_dout;
wire sig_group_delete_DMA_In_V_last_V_empty_n;
wire sig_group_delete_DMA_In_V_last_V_read;

wire [32 - 1:0] sig_group_delete_DMA_Out_V_data_V_din;
wire sig_group_delete_DMA_Out_V_data_V_full_n;
wire sig_group_delete_DMA_Out_V_data_V_write;
wire [1 - 1:0] sig_group_delete_DMA_Out_V_last_V_din;
wire sig_group_delete_DMA_Out_V_last_V_full_n;
wire sig_group_delete_DMA_Out_V_last_V_write;

wire sig_group_delete_ap_rst;



group_delete group_delete_U(
    .DMA_In_V_data_V_dout(sig_group_delete_DMA_In_V_data_V_dout),
    .DMA_In_V_data_V_empty_n(sig_group_delete_DMA_In_V_data_V_empty_n),
    .DMA_In_V_data_V_read(sig_group_delete_DMA_In_V_data_V_read),
    .DMA_In_V_last_V_dout(sig_group_delete_DMA_In_V_last_V_dout),
    .DMA_In_V_last_V_empty_n(sig_group_delete_DMA_In_V_last_V_empty_n),
    .DMA_In_V_last_V_read(sig_group_delete_DMA_In_V_last_V_read),
    .DMA_Out_V_data_V_din(sig_group_delete_DMA_Out_V_data_V_din),
    .DMA_Out_V_data_V_full_n(sig_group_delete_DMA_Out_V_data_V_full_n),
    .DMA_Out_V_data_V_write(sig_group_delete_DMA_Out_V_data_V_write),
    .DMA_Out_V_last_V_din(sig_group_delete_DMA_Out_V_last_V_din),
    .DMA_Out_V_last_V_full_n(sig_group_delete_DMA_Out_V_last_V_full_n),
    .DMA_Out_V_last_V_write(sig_group_delete_DMA_Out_V_last_V_write),
    .ap_rst(sig_group_delete_ap_rst),
    .ap_clk(aclk)
);

group_delete_DMA_In_if group_delete_DMA_In_if_U(
    .ACLK(aclk),
    .ARESETN(aresetn),
    .DMA_In_V_data_V_dout(sig_group_delete_DMA_In_V_data_V_dout),
    .DMA_In_V_data_V_empty_n(sig_group_delete_DMA_In_V_data_V_empty_n),
    .DMA_In_V_data_V_read(sig_group_delete_DMA_In_V_data_V_read),
    .DMA_In_V_last_V_dout(sig_group_delete_DMA_In_V_last_V_dout),
    .DMA_In_V_last_V_empty_n(sig_group_delete_DMA_In_V_last_V_empty_n),
    .DMA_In_V_last_V_read(sig_group_delete_DMA_In_V_last_V_read),
    .TVALID(DMA_In_TVALID),
    .TREADY(DMA_In_TREADY),
    .TDATA(DMA_In_TDATA),
    .TLAST(DMA_In_TLAST));

group_delete_DMA_Out_if group_delete_DMA_Out_if_U(
    .ACLK(aclk),
    .ARESETN(aresetn),
    .DMA_Out_V_data_V_din(sig_group_delete_DMA_Out_V_data_V_din),
    .DMA_Out_V_data_V_full_n(sig_group_delete_DMA_Out_V_data_V_full_n),
    .DMA_Out_V_data_V_write(sig_group_delete_DMA_Out_V_data_V_write),
    .DMA_Out_V_last_V_din(sig_group_delete_DMA_Out_V_last_V_din),
    .DMA_Out_V_last_V_full_n(sig_group_delete_DMA_Out_V_last_V_full_n),
    .DMA_Out_V_last_V_write(sig_group_delete_DMA_Out_V_last_V_write),
    .TVALID(DMA_Out_TVALID),
    .TREADY(DMA_Out_TREADY),
    .TDATA(DMA_Out_TDATA),
    .TLAST(DMA_Out_TLAST));

group_delete_ap_rst_if #(
    .RESET_ACTIVE_LOW(RESET_ACTIVE_LOW))
ap_rst_if_U(
    .dout(sig_group_delete_ap_rst),
    .din(aresetn));

endmodule
