/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>

#include "platform.h"
#include "xaxidma.h"
#include "xscutimer.h"
#include "xparameters.h"
#include "xtime_l.h"
#include "ff.h"

#include "ppm_lib.h"


#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif

#define NSECS_PER_SEC          667000000/2
#define TIMER_RES_DIVIDER      40
#define EE_TICKS_PER_SEC       (NSECS_PER_SEC / TIMER_RES_DIVIDER)
#define TIMER_LOAD_VALUE      0xFFFFFFE

int init_dma(XAxiDma *axiDmaPtr, int deviceId) {
	XAxiDma_Config *CfgPtr;
	int status;
	// Get pointer to DMA configuration
	CfgPtr = XAxiDma_LookupConfig(deviceId);
	if (!CfgPtr) {
		xil_printf("Error looking for AXI DMA config\n\r");
		return XST_FAILURE;
	}
	// Initialize the DMA handle
	status = XAxiDma_CfgInitialize(axiDmaPtr, CfgPtr);
	if (status != XST_SUCCESS) {
		xil_printf("Error initializing DMA\n\r");
		return XST_FAILURE;
	}
	//check for scatter gather mode - this example must have simple mode only
	if (XAxiDma_HasSg(axiDmaPtr)) {
		xil_printf("Error DMA configured in SG mode\n\r");
		return XST_FAILURE;
	}
	//disable the interrupts
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

	return XST_SUCCESS;
}

#define NMAX 2097151
#define D_TYPE int

#define WIDTH 565
#define HEIGHT 584
#define MAX_GROUPS 10000

XAxiDma AxiDma0;
XAxiDma AxiDma1;
int Status;

#define OUTPUT_LOG


int first_tosend = 0;

int first = 1;

void filter(int *input,int *output, int width, int height) {
	XTime startHW, endHW;
	int counts[MAX_GROUPS] = { 0 };
	float timeHW;

	int nmax = NMAX;
	int nmax_check;

	int n = width * height;

	//Allocate partial output buffer
	int *output_partial = (int*) malloc(sizeof(int)*width*height);
	if(!output_partial){
		xil_printf("Error in partial output malloc\r\n");
		return;
	}

	XTime_GetTime(&startHW);



	/********************************************************
	 * FIRST DMA -> VESSELS SEGMENTATION AND BLOB DETECTION *
	 ********************************************************/

	//xil_printf("Sending nmax setting\r\n");
	//Transfer NMAX  parameter
	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) &nmax, sizeof(int),
	XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail nmax setting!!\r\n");
	}
	while ((XAxiDma_Busy(&AxiDma0, XAXIDMA_DMA_TO_DEVICE)))
		;

	//receives and check NMAX parameter
	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) &nmax_check, sizeof(int),
	XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail nmax receiving!!\r\n");
	}
	while (XAxiDma_Busy(&AxiDma0, XAXIDMA_DEVICE_TO_DMA))
		;

	if (nmax_check != nmax) {
		xil_printf(
				"Failed NMAX parameter propagation\r\nPassed: %d \t Received:%d\r\n",
				nmax, nmax_check);
	} else {
		xil_printf("Parameters passed\r\n");
	}

	//xil_printf("Sending first pixels\r\n");


	//send and receive the image to the first DMA

	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) input, (n) * sizeof(D_TYPE),
	XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail first send!\n");
	}
	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) output_partial, (n) * sizeof(D_TYPE),
	XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail first read!\n");
	}


	while (XAxiDma_Busy(&AxiDma0, XAXIDMA_DEVICE_TO_DMA) || (XAxiDma_Busy(&AxiDma0, XAXIDMA_DMA_TO_DEVICE)));

	//receives groups

	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) counts,
			(10000) * sizeof(D_TYPE), XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail receiving counts\r\n");
	}

	while (XAxiDma_Busy(&AxiDma0, XAXIDMA_DMA_TO_DEVICE)
			|| XAxiDma_Busy(&AxiDma0, XAXIDMA_DEVICE_TO_DMA))
		;


	/********************************************************
	 *      SECOND DMA -> BLOB REMOVAL AND POSTPROCESSING   *
	 ********************************************************/


	//xil_printf("Group delete filter");

	//sends and receives from second dma
	nmax_check = 0;
	//xil_printf("Sending nmax setting to second dma\r\n");
	//while (XAxiDma_Busy(&AxiDma1, XAXIDMA_DEVICE_TO_DMA)||(XAxiDma_Busy(&AxiDma1, XAXIDMA_DMA_TO_DEVICE)));

	//Transfer NMAX parameter and checks it

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) &nmax, sizeof(D_TYPE),
	XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail nmax setting!! %d\r\n", Status);

	}

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) &nmax_check, sizeof(D_TYPE),
	XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail nmax receiving!!  %d\r\n", Status);

	}

	while (XAxiDma_Busy(&AxiDma1, XAXIDMA_DMA_TO_DEVICE)
			|| XAxiDma_Busy(&AxiDma1, XAXIDMA_DEVICE_TO_DMA))
		;

	if (nmax_check != nmax) {
		xil_printf(
				"Failed NMAX parameter propagation\r\nPassed: %d \t Received:%d\r\n",
				nmax, nmax_check);
		return;
	}

	//sends groups array

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) counts,
			(10000) * sizeof(D_TYPE), XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail sending counts\r\n");
	}

	while (XAxiDma_Busy(&AxiDma1, XAXIDMA_DMA_TO_DEVICE))
		;

	//send and receive the image to filter

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) output,(n) * sizeof(D_TYPE), XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail first read!\n");
	}

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) output_partial, (n) * sizeof(D_TYPE), XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail first send!\n");
	}

	while (XAxiDma_Busy(&AxiDma1, XAXIDMA_DMA_TO_DEVICE)
			|| (XAxiDma_Busy(&AxiDma1, XAXIDMA_DEVICE_TO_DMA)))
		;

	XTime_GetTime(&endHW);

	free(output_partial);

	//compute time and print it

	timeHW = 1.0 * (endHW - startHW) / (NSECS_PER_SEC);

	printf("%.5f\r\n", timeHW);
}


int main() {

	char *input_name = "input";
	char *output_name = "output";

	int *input;
	int *output;
	int width,height;

	init_platform();

	xil_printf("\r\n--- Input array --- \r\n");

	//Disable cache to not have issues using DMAs and filesystem procedures
	Xil_DCacheDisable();
	Xil_ICacheDisable();

	// init DMAs
	Status = init_dma(&AxiDma0, XPAR_AXIDMA_0_DEVICE_ID);
	if(Status!=XST_SUCCESS){
		xil_printf("DMA del cazzo\r\n");
		return XST_FAILURE;
	}
	Status = init_dma(&AxiDma1, XPAR_AXIDMA_1_DEVICE_ID);
	if(Status!=XST_SUCCESS){
		xil_printf("DMA del cazzo\r\n");
		return XST_FAILURE;
	}

	xil_printf("DMA initialized\r\n");

	xil_printf("Generating Hardware output\r\n");

	//init filesystem
	init_fs();

	//load input image
	input = load_ppm_bin(input_name,&width,&height,GREEN);

	//allocate output buffer
	output = aligned_malloc(sizeof(int)*width*height);
	if(output==NULL){
		xil_printf("Error in allocating output buffer\r\n");
		return XST_FAILURE;
	}

	//perform vessels segmentation
	filter(input,output,width,height);

	//save output file
	save_ppm_bin(output_name,width,height,output);


	xil_printf("\r\nFinish, no errors\r\n");
	return 0;
}
