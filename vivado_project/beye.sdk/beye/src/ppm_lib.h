#ifndef PPM_LIB
#define PPM_LIB

#include <stdio.h>
#include <stdlib.h>
#include "platform.h"
#include "xaxidma.h"
#include "xscutimer.h"
#include "xparameters.h"
#include "xtime_l.h"
#include "ff.h"


#define TEXTBUF_LENGTH 50

#define NL 0x0A
#define SPACE 0x20

#define RED		0
#define GREEN	1
#define BLUE	2

int init_fs();

void *aligned_malloc(size_t size);

int *load_ppm_bin(char *filename, int *width,int *height,int channel);

int save_ppm_bin(char *filename, int width,int height, int *array);

#endif
