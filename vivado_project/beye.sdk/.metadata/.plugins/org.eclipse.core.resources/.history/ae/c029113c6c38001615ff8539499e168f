/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#define WIDTH 565
#define HEIGHT 584

#include <stdio.h>
#include <stdlib.h>

#include "platform.h"
#include "xaxidma.h"
#include "xscutimer.h"
#include "xparameters.h"
#include "xtime_l.h"
#include "ff.h"

#include "ppm_lib.h"

//#define FILE_INPUT 1
#define N  (WIDTH*HEIGHT)
//#include "mat_in.h"
int input[N] __attribute__ ((aligned(32)));

#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif

#define NSECS_PER_SEC          667000000/2
#define TIMER_RES_DIVIDER      40
#define EE_TICKS_PER_SEC       (NSECS_PER_SEC / TIMER_RES_DIVIDER)
#define TIMER_LOAD_VALUE      0xFFFFFFE

int init_dma(XAxiDma *axiDmaPtr, int deviceId) {
	XAxiDma_Config *CfgPtr;
	int status;
	// Get pointer to DMA configuration
	CfgPtr = XAxiDma_LookupConfig(deviceId);
	if (!CfgPtr) {
		xil_printf("Error looking for AXI DMA config\n\r");
		return XST_FAILURE;
	}
	// Initialize the DMA handle
	status = XAxiDma_CfgInitialize(axiDmaPtr, CfgPtr);
	if (status != XST_SUCCESS) {
		xil_printf("Error initializing DMA\n\r");
		return XST_FAILURE;
	}
	//check for scatter gather mode - this example must have simple mode only
	if (XAxiDma_HasSg(axiDmaPtr)) {
		xil_printf("Error DMA configured in SG mode\n\r");
		return XST_FAILURE;
	}
	//disable the interrupts
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

	return XST_SUCCESS;
}

#define NMAX 2097151
#define D_TYPE int
#define TSTEPS 1000
#define NSTEPS N/NMAX
#define KERN_SIZE 3
#define CENTER (KERN_SIZE/2)

#define OFFSET ((CENTER-1+ KERN_SIZE%2)*WIDTH + CENTER-(1-KERN_SIZE%2)) //19447//25930//((KERN_SIZE-1)*WIDTH+KERN_SIZE)/2

#define STEPSTOT (WIDTH*HEIGHT/NMAX)
#define INCR (STEPSTOT/50)

XAxiDma AxiDma0;
XAxiDma AxiDma1;
int Status;

#define OUTPUT_LOG


int first_tosend = 0;

int first = 1;

void filter(int *input,int *output, int width, int height) {
	XTime startHW, endHW;
	int counts[10000] = { 0 };
	float timeHW;

	int nmax = NMAX;
	int nmax_check;

	int n = width * height;

	int *output_partial = (int*) malloc(sizeof(int)*width*height);
	if(!output_partial){
		xil_printf("Error in partial output malloc\r\n");
		return;
	}

	XTime_GetTime(&startHW);

	xil_printf("Sending nmax setting\r\n");
	//Transfer parameter
	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) &nmax, sizeof(int),
	XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail nmax setting!!\r\n");
	}
	while ((XAxiDma_Busy(&AxiDma0, XAXIDMA_DMA_TO_DEVICE)))
		;

	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) &nmax_check, sizeof(int),
	XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail nmax receiving!!\r\n");
	}
	while (XAxiDma_Busy(&AxiDma0, XAXIDMA_DEVICE_TO_DMA))
		;

	if (nmax_check != nmax) {
		xil_printf(
				"Failed NMAX parameter propagation\r\nPassed: %d \t Received:%d\r\n",
				nmax, nmax_check);
	} else {
		xil_printf("Parameters passed\r\n");
	}

	xil_printf("Sending first pixels\r\n");

	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) input, (n) * sizeof(D_TYPE),
	XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail first send!\n");
	}
	//First transfer, used to open the receiving channel
	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) output_partial, (n) * sizeof(D_TYPE),
	XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail first read!\n");
	}

	//receives groups
	while (XAxiDma_Busy(&AxiDma0, XAXIDMA_DEVICE_TO_DMA)
			|| (XAxiDma_Busy(&AxiDma0, XAXIDMA_DMA_TO_DEVICE)))
		;

	Status = XAxiDma_SimpleTransfer(&AxiDma0, (u32) counts,
			(10000) * sizeof(D_TYPE), XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail receiving counts\r\n");
	}

	while (XAxiDma_Busy(&AxiDma0, XAXIDMA_DMA_TO_DEVICE)
			|| XAxiDma_Busy(&AxiDma0, XAXIDMA_DEVICE_TO_DMA))
		;


	xil_printf("Group delete filter");

	//sends and receives from second dma
	nmax_check = 0;
	xil_printf("Sending nmax setting to second dma\r\n");
	//while (XAxiDma_Busy(&AxiDma1, XAXIDMA_DEVICE_TO_DMA)||(XAxiDma_Busy(&AxiDma1, XAXIDMA_DMA_TO_DEVICE)));

	//Transfer parameter

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) &nmax, sizeof(D_TYPE),
	XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail nmax setting!! %d\r\n", Status);

	}

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) &nmax_check, sizeof(D_TYPE),
	XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail nmax receiving!!  %d\r\n", Status);

	}

	while (XAxiDma_Busy(&AxiDma1, XAXIDMA_DMA_TO_DEVICE)
			|| XAxiDma_Busy(&AxiDma1, XAXIDMA_DEVICE_TO_DMA))
		;

	if (nmax_check != nmax) {
		xil_printf(
				"Failed NMAX parameter propagation\r\nPassed: %d \t Received:%d\r\n",
				nmax, nmax_check);
		return;
	}

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) counts,
			(10000) * sizeof(D_TYPE), XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail sending counts\r\n");
	}

	while (XAxiDma_Busy(&AxiDma1, XAXIDMA_DMA_TO_DEVICE))
		;

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) output,(n) * sizeof(D_TYPE), XAXIDMA_DEVICE_TO_DMA);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail first read!\n");
	}

	Status = XAxiDma_SimpleTransfer(&AxiDma1, (u32) output_partial, (n) * sizeof(D_TYPE), XAXIDMA_DMA_TO_DEVICE);
	if (Status != XST_SUCCESS) {
		xil_printf("Fail first send!\n");
	}

	while (XAxiDma_Busy(&AxiDma1, XAXIDMA_DMA_TO_DEVICE)
			|| (XAxiDma_Busy(&AxiDma1, XAXIDMA_DEVICE_TO_DMA)))
		;

	XTime_GetTime(&endHW);

	timeHW = 1.0 * (endHW - startHW) / (NSECS_PER_SEC);

	printf("%.5f\r\n", timeHW);
}

#define NL 0x0A
#define SPACE 0x20
#define MAX_VAL_LENGTH 10

#define RED 0
#define GREEN 1
#define BLUE 2

/*static FATFS fatfs;
FRESULT Res;
TCHAR *Path = "0:/";*/

/*
void fai_cose() {
	uint8_t io_array[N * 3 + 15] __attribute__ ((aligned(32)));

	char *in_name = "right";
	char *out_name = "ciao";
	int bytes_read;
	uint8_t primi_17[15] __attribute__ ((aligned(32)))
	= "P5\n565 584\n255\n";

	int i, j;

	FIL file;

	xil_printf("Reading image from file \r\n");

	Res = f_open(&file, in_name, FA_READ);
	if (Res) {
		xil_printf("Input file error, %d\r\n", Res);
		return XST_FAILURE;
	}

	Res = f_read(&file, io_array, sizeof(uint8_t) * (N * 3 + 15), &bytes_read);
	if (Res) {
		xil_printf("Error in reading pixels from file\r\n");
		return XST_FAILURE;
	}

	xil_printf("Letti %d bytes, arrivato a %d\r\n", bytes_read, f_tell(&file));

	Res = f_close(&file);

	xil_printf("Chiuso input file, %d\r\n", Res);

	xil_printf("Copio in input...\r\n");

	for(i=0;i<15;i++){
	 primi_17[i] = io_array[i];
	 printf("%c ",io_array[i]);
	 }


	j = 0;
	for (i = 15; i < (N * 3 + 15); i++) {
		if ((i - 15) % 3 == 1) {
			input[j] = io_array[i];
			j++;
		}
	}

	int t;



	xil_printf("Filtro...\r\n");
	filter();

	xil_printf("Copio output...\r\n");

	for (i = 0; i < 15; i++) {
		io_array[i] = primi_17[i];
		printf("%c ", primi_17[i]);
	}

	j = 0;
	for (i = 15; i < (N + 15); i++) {
		io_array[i] = output_tot[j];
		j++;
	}
	xil_printf("\r\n");

	Res = f_open(&file, out_name, FA_CREATE_ALWAYS | FA_WRITE);
	if (Res) {
		xil_printf("Failed to open output file, %d\r\n", Res);
		return XST_FAILURE;
	}

	Res = f_write(&file, (const void*) io_array, sizeof(uint8_t) * (N + 15),
			&bytes_read);
	if (Res) {
		xil_printf("Failed to write output pixels\r\n");
		return XST_FAILURE;
	}

	xil_printf("Scritti %d bytes, arrivato a %d\r\n", bytes_read,
			f_tell(&file));

	Res = f_sync(&file);

	xil_printf("Result of syncing: %d\r\n", Res);

	Res = f_close(&file);

	xil_printf("Result of closing: %d\r\n", Res);

}
*/

int main() {

	char *input_name = "right";
	char *output_name = "ciao";

	int *input;
	int *output;
	int width,height;

	init_platform();

	xil_printf("\r\n--- Input array --- \r\n");

	Xil_DCacheDisable();
	Xil_ICacheDisable();

	// init DMA
	Status = init_dma(&AxiDma0, XPAR_AXIDMA_0_DEVICE_ID);
	if(Status!=XST_SUCCESS){
		xil_printf("DMA del cazzo\r\n");
		return XST_FAILURE;
	}
	Status = init_dma(&AxiDma1, XPAR_AXIDMA_1_DEVICE_ID);
	if(Status!=XST_SUCCESS){
		xil_printf("DMA del cazzo\r\n");
		return XST_FAILURE;
	}

	xil_printf("DMA initialized\r\n");

	xil_printf("Generating Hardware output\r\n");

	init_fs();

	load_ppm_bin(input_name,&width,&height,&input);

	filter(input,output,width,height);

	save_ppm_bin(output_name,width,height,output);


	xil_printf("\r\nFinish, no errors\r\n");
	return 0;
}
