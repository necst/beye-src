##BEye

Team number: XIL-­91291
Project name:BEye
Date:04/07/2016
Version of uploaded archive: 1

##Politecnico di Milano

Participants: Marco Bacis, Lara Cavinato, Irene Fidone
Email: marco.bacis@mail.polimi.it, lara.cavinato@mail.polimi.it, irene.fidone@mail.polimi.it

Supervisor name: Marco Domenico Santambrogio
Supervisor e-mail: marco.santambrogio@polimi.it

Board used: ZedBoard
Vivado Version: 2015.3


###Brief description of project:

Fundus photography is a test used in ophthalmology to detect and prevent eyes diseases
involving also retina, optic nerve and vitreous. Some diseases include macular degeneration,
glaucoma, edema and ocular diabetes. The test consists of capturing a photograph of the back
of the eye, using a digital ophthalmoscope, and examining it. In particular, retinal vessels
microcirculation plays a major role in the test. 
Unfortunately, segmentation and mapping of retinal vessels is a computationally hard task, 
and this can lead to slow or inaccurate results.
Within this context it can be easily imagined the great benefit it can be obtained in being able
at speeding up this process. In this work, we present an FPGA system (implemented on Zynq
platform) able to extract and filter blood vessels from a given retinal image. This is done in
order to improve the performance, in terms of energy efficiency, with respect to state of the art
segmentation algorithms



###Project repository structure
The repository/archive consists of the following directories:

/doc - location of the design documentation file (beye_project_report.pdf)
/ip - location of the Vivado HLS cores sources
--> /ip/adaptive_threshold
--> /ip/circular_bound
--> /ip/gauss
--> /ip/group_delete
--> /ip/group_filter
--> /ip/match_filter
--> /ip/median_filter
/vivado_project - Vivado/SDK project directory
/software_implementation - location of the python implementation of the proposed algorithm


###Instructions to build and test project

Step 1: For each directory in /cores, run "vivado_hls script.tcl" to create each core vivado hls project and synthesize it

Step 2: Open the vivado project using Vivado. Then, open the project's block design and perform a "Report Status" and upgrade of the design's ip, previously generated with Vivado HLS.

Step 3: With Vivado, perform Synthesis,implementation and bitstream generation of the design. Then, click File->Export Hardware (checking "include bitstream")

Step 4: Format a SD card in FAT32 and copy the input image (input.ppm) present into software_implementation directory, then insert the SD card into Zedboard, then switch on the board.

Step 5: Click File->Launch SDK to launch Vivado SDK. To run the application, program it with "Xilinx Tools->Program FPGA". After this, open a serial terminal (gtkterm, screen or SDK's one), and connect to the ZedBoard UART (at 115200 bauds).

Step 6: Launch the application from SDK, then wait for it to end. You can see the algorithm steps and time on the terminal. Finally, switch off the board. On the SD card there will be a file named "OUTPUT" with the vessels segmentation output image.


###Youtube Videos

BEye playlist: [link] (https://www.youtube.com/playlist?list=PLewc2qlpcOueh9xMhoDR7G93k6ZZ3pDrO)

Main/Final demo video: https://youtu.be/B-yQHMwHsF0

Other videos:
1. [Project Presentation] (https://youtu.be/xzR7rLOviY8?list=PLewc2qlpcOueh9xMhoDR7G93k6ZZ3pDrO)
2. [Rationale behind an FPGA-based implementation] (https://youtu.be/5Ia_QoEIyiE?list=PLewc2qlpcOueh9xMhoDR7G93k6ZZ3pDrO)
3. [Work organization] (https://youtu.be/3tVh6KpcjwY?list=PLewc2qlpcOueh9xMhoDR7G93k6ZZ3pDrO)
4. [Team presentation] (https://youtu.be/JywewBwBsr8?list=PLewc2qlpcOueh9xMhoDR7G93k6ZZ3pDrO)
5. [Market analysis] (https://youtu.be/Cd9pTUQ0m1s?list=PLewc2qlpcOueh9xMhoDR7G93k6ZZ3pDrO)

