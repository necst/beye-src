
 # Copyright 2016 Lara Cavinato, Irene Fidone, Marco Bacis
  
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
  
 # http://www.apache.org/licenses/LICENSE-2.0
  
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

import sys
import matplotlib.pyplot as plt
import cv2
import numpy as np
import os
from scipy.cluster import vq
from numpy import reshape,uint8,flipud
from scipy.cluster.vq import kmeans,vq
from pylab import imread,imshow,figure,show,subplot

#our libraries
import filters as filters
import DetectVessels as detection
import support as sp 
import diff_function as diffLib

def main():


    #########################PARAMETERS SET UP
    for z in range (1 ,21):
        img = cv2.imread('input/%s_test.tif' % (z))

        img_ok= cv2.imread('manual/%s_manual1.jpg' % (z), 0)

        r,g,b = cv2.split(img)

        img_name = 'input/%s_test.tif' % (z)

        img_ok_name = 'manual/%s_manual1.jpg' % (z)

        g=cv2.medianBlur(g,3)

        g = detection.DetectVessels(g)

        blur = cv2.GaussianBlur(g,(5,5),0)


    
        th4 = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,9,3)
        th4 = filters.findVesselsStructure(th4,100)
        th4 = filters.deleteCircularBound(th4)
    

    
                
        noring=filters.deleteCircularBound (th4) 
        cv2.imwrite("output/findvessels.tif", noring)

        final=filters.postprocessing(noring)
        cv2.imwrite("output/final.tif", final)



        sens,spec,acc = diffLib.diff(img_ok, final)

        print  "Sensitivity:" + str(sens)
        print  "Specificity:" + str(spec)
        print  "Accuracy:" + str(acc)  

        


if __name__ == "__main__":
    main()

    
