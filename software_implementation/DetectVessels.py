
 # Copyright 2016 Synelixis Solutions Ltd.

 # Description: Retinal Vessel Detection application use case for the EXTRA EU Project
 # Project Name: EXTRA-H2020
 # Author:  Antonis Nikitakis, nikitakis@synelixis.com


 # Copyright 2016 Lara Cavinato, Irene Fidone, Marco Bacis
  
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
  
 # http://www.apache.org/licenses/LICENSE-2.0
  
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.



from __future__ import division
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import math
import cv2
import sys  
import time  
from scipy import ndimage
from skimage.morphology import watershed
from skimage.morphology import skeletonize
import operator
import support as sp



def DetectVessels(img, kernel_divide=30):
    print "starting Detect Vessels"

    # MAX RESPONSE

    kernel_zero = np.zeros(shape=(1, 16), dtype="int")
    kernel_line = np.zeros(shape=(1, 16), dtype="int")
    kernel_line = np.array(
        [0, 4, 3, 2, 1, -2, -5, -6, -5, -2, 1, 2, 3, 4, 0, 0])
    kernel_line.shape = (1, 16)

    kernel = [kernel_zero,
            kernel_zero,
            kernel_zero,
            kernel_line,
            kernel_line,
            kernel_line,
            kernel_line,
            kernel_line,
            kernel_line,
            kernel_line,
            kernel_line,
            kernel_line,
            kernel_line,
            kernel_zero,
            kernel_zero,
            kernel_zero]

    kernel = np.asarray(kernel).reshape((16, 16))
    

    thetas = [0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165, 180]

    x, y = img.shape
    dst = np.ndarray(shape=(x, y), dtype="uint8")
    rot_kernel = np.zeros(shape=(16, 16), dtype="int")

    kernel = np.uint8(kernel + 10)
    rot_kernels = []
    responses = list()
    for theta in thetas:  # np.arange(0, 180 ,30):
        M = cv2.getRotationMatrix2D((8, 8), theta, 1)   # cols/2,rows/2 defines the center of  rotation, last argument is scale
        rot_kernel = cv2.warpAffine(kernel, M, (16, 16), borderValue=10)  # Rotation is done
        
        rot_kernel = (rot_kernel.astype(int) - 10).astype(int)
        
        width, height = rot_kernel.shape

        rot_kernel = rot_kernel / float(kernel_divide)

        rot_kernels.append(rot_kernel)

        dst = cv2.filter2D(img, -1, rot_kernel)
        
        responses.append(dst)  # append tuple

    responses = np.asarray(responses)

    max_response = np.zeros(shape=(x, y), dtype="uint8")
    max_response_theta = np.zeros(shape=(x, y), dtype="uint8")
    max_pix = -1
    for x_pix in range(x):
        for y_pix in range(y):
            for z_pix in range(len(thetas)):
                if responses[z_pix][x_pix][y_pix] > max_pix:
                    max_pix = responses[z_pix][x_pix][y_pix]
                    max_z = z_pix

            max_response[x_pix][y_pix] = max_pix
            max_response_theta[x_pix][y_pix] = max_z * 30
            max_pix = -1
     

    maxresponse_final = (255 - max_response)

    return maxresponse_final