
 # Copyright 2016 Lara Cavinato, Irene Fidone, Marco Bacis
  
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
  
 # http://www.apache.org/licenses/LICENSE-2.0
  
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

from __future__ import division
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import math
import cv2
import sys  
import time  
from scipy import ndimage
from skimage.morphology import watershed
from skimage.morphology import skeletonize
import operator
import support as sp


#function that takes a greyscale matrix (image)
#and delete black pixel's groups smaller than "threshold" pixels
def findVesselsStructure(img,threshold=100):
    
    r, c = img.shape

    matrix = np.zeros((r,c))
    a = 1

    #group directory
    groups = {}
    groups[0]=0

    for i in range(0, r):
        for j in range(0, c):

            if(img[i][j] == 0):
                up = matrix[i - 1][j]
                left = matrix[i][j-1]

                #if up and left are null, assign new group
                if (up==0) and (left==0):
                    tmp = a
                    groups[a]=0
                    a += 1
                    
                elif(up!=0):
                    #if left and up aren't equals, append the pair
                    if(left!=0 and left!=up):
                        
                        #adds the left group to the up "supergroup"
                        #like a link between the two groups

                        if(groups[up]!=0):
                            groups[left] = groups[up]
                        else:
                            groups[left]=up
                        tmp = left
                    else:
                        #otherwise just assign the pixel to the upper group
                        tmp = up
                else:
                    #if only left is available, assign it
                    tmp = left

                matrix[i][j] = tmp
            else:
                matrix[i][j]=0

    #counting
    counts = np.zeros(len(groups))
    for i in range(0, r):
        for j in range(0, c):
            #counts based on the link

            #"supergroup" (has 0 on its entry)
            if(groups[matrix[i][j]]==0):
                counts[matrix[i][j]]+=1

            #minor group (has the supergroup id as value)
            else:
                counts[groups[matrix[i][j]]]+=1

    
    #read the count and decide to blank the pixel
    #based on the same rule as the loop above

    for i in range(0, r):
        for j in range(0, c):

            if(groups[matrix[i][j]]==0):
                tot = counts[matrix[i][j]]
            else:
                tot = counts[groups[matrix[i][j]]]

            #delete the pixel directly from the image matrix
            if(tot<threshold):
                img[i][j] = 255

    return img


def deleteCircularBound(img) :
    
    r, c = img.shape

    d  = int(c/2-10)
    c1 = int(c/2)
    r1 = int(r/2)
    
    cv2.circle(img, (c1, r1), d, 255 , 25)
    
    return img

def postprocessing (img) :  
    th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,9,3)
    final  =cv2.medianBlur(th3,5)

    return final

