
 # Copyright 2016 Lara Cavinato, Irene Fidone, Marco Bacis
  
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
  
 # http://www.apache.org/licenses/LICENSE-2.0
  
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import math
import cv2
from scipy import ndimage
from skimage.morphology import watershed
from skimage.morphology import skeletonize
import operator


def GammaCorrection(img, correction):
    img = img/255.0
    img = cv2.pow(img, correction)
    return np.uint8(img*255)

def select(sx, su):
    if su:
        return su
    return sx

def common(p1, p2):
    for i in p1:
        for j in p2:
            if i[1] == j[1]:
                return True
    return False


def diff(img_ok,img_sw):
    
    dimx,dimy = img_ok.shape
    dimx1,dimy1 = img_sw.shape

    #print str(dimx) + " " + str(dimy) + " " + str(dimx1) + " " + str(dimy1) 

    if((dimx!=dimx1) or (dimy!=dimy1)):
        print "Error, different dimensions"

    count = 0
    ok_pix = 0
    sw_pix = 0

    for y in range(dimy):
        for x in range(dimx):
            ok_pix = 255 if img_ok[x][y]>128 else 0 
            sw_pix = 0 if img_sw[x][y]>128 else 255

            if(ok_pix==sw_pix):
                count+=1

    result = count*100 / (dimx*dimy)

    return result