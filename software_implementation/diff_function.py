
 # Copyright 2016 Lara Cavinato, Irene Fidone, Marco Bacis
  
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
  
 # http://www.apache.org/licenses/LICENSE-2.0
  
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

from __future__ import division
import sys
import matplotlib.pyplot as plt
import cv2
import numpy as np
import scipy
from matplotlib import pyplot as plt

def diff(img_ok,img_sw):

    dimx, dimy = img_ok.shape
    dimx1,dimy1 = img_sw.shape
    
    if((dimx!=dimx1) or (dimy!=dimy1)):
        print "Error, different dimensions"
    

    count = 0
    ok_pix = 0
    sw_pix = 0

    for y in range(dimy):
        for x in range(dimx):
            ok_pix = 255 if img_ok[x][y]>128 else 0 
            sw_pix = 0 if img_sw[x][y]>128 else 255

            if(ok_pix==sw_pix):
                count+=1

    result1 = count*100 / (dimx*dimy)

    
    

    for y in range(dimy):
        for x in range(dimx):
            if img_ok[x][y]>128 : 
                img_ok[x][y] = 255 
            else :
                img_ok[x][y]=0 
        

            
    for y in range(dimy):
        for x in range(dimx):
            if img_sw[x][y]==0: 
                img_sw[x][y]=255
            else :
                img_sw[x][y]=0

    tn=0
    tp=0
    fn=0
    fp=0

    for y in range(dimy):
        for x in range(dimx):
            if img_sw[x][y]==img_ok[x][y]==255:
                tp+=1
            elif img_sw[x][y]==img_ok[x][y]==0:
                tn+=1
            elif img_sw[x][y]==255 and img_ok[x][y]==0:
                fp+=1
            elif img_sw[x][y]==0 and img_ok[x][y]==255:
                fn+=1

    sensitivity = tp /(tp+fn)
    specificity= tn*100/(tn+fp)
    accuracy= (tp+tn)/(dimx*dimy)

    return sensitivity, specificity, accuracy





